import { parse } from 'node-html-parser?server'

function parseHTML (html) {
  if (import.meta.env.SSR) {
    return parse(html)
  } else {
    const div = document.createElement('div')
    div.innerHTML = html

    return div
  }
}

function getChildNodes (node) {
  return node.childNodes
}

function getAttribute (node, key) {
  return node.getAttribute(key)
}

function getClass (node) {
  return node.className
}

function getText (node) {
  if (import.meta.env.SSR) {
    return node.rawText
  } else {
    return node.textContent
  }
}

function nodeIs (node, tagName) {
  if (tagName === 'TEXT') {
    return node.nodeType === 3
  }

  return node.tagName === tagName
}

export {
  parseHTML,

  getChildNodes,
  getClass,
  getAttribute,
  getText,
  nodeIs
}
