function buildHead (options) {
  const { title, description, image, url } = options

  const result = buildHeadTitle(title)

  if (url) {
    result.meta.push({ property: 'og:url', content: url })
  }

  if (description) {
    result.meta = result.meta.concat(buildHeadDescriptionMeta(description))
  }

  if (image) {
    result.meta = result.meta.concat(buildHeadImageMeta(image))
  }

  return result
}

function buildHeadTitle (titleArg) {
  const title = titleArg + ' | JoinPeerTube'

  return {
    title,

    meta: [
      {
        property: 'og:title',
        content: title
      },
      {
        name: 'twitter:title',
        content: title
      }
    ]
  }
}

function buildHeadDescriptionMeta (description) {
  return [
    {
      name: 'description',
      content: description
    },
    {
      property: 'og:description',
      content: description
    },
    {
      name: 'twitter:description',
      content: description
    }
  ]
}

function buildHeadImageMeta (image) {
  return [
    {
      property: 'og:image',
      content: image
    },
    {
      name: 'twitter:image',
      content: image
    }
  ]
}

function buildHeadArticle (options) {
  const { publishedDate } = options

  const result = buildHead(options)

  result.meta = result.meta.concat([
    {
      property: 'og:type',
      content: 'article'
    },
    {
      property: 'article:published_time',
      content: publishedDate.toISOString()
    },
    {
      property: 'article:modified_time',
      content: publishedDate.toISOString()
    }
  ])

  return result
}

export {
  buildHead,

  buildHeadTitle,
  buildHeadDescriptionMeta,
  buildHeadImageMeta,

  buildHeadArticle
}
