import axios from 'axios'

function httpGet (url, params) {
  return axios.get(url, { params })
    .then(({ data }) => data)
}

export {
  httpGet
}
