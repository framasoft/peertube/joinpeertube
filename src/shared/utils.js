function durationToString (duration) {
  const hours = Math.floor(duration / 3600)
  const minutes = Math.floor((duration % 3600) / 60)
  const seconds = duration % 60

  const minutesPadding = minutes >= 10 ? '' : '0'
  const secondsPadding = seconds >= 10 ? '' : '0'
  const displayedHours = hours > 0 ? hours.toString() + ':' : ''

  return (
    displayedHours + minutesPadding + minutes.toString() + ':' + secondsPadding + seconds.toString()
  ).replace(/^0/, '')
}

function findAppropriateImageByWidth (images, minWidth) {
  if (images.length === 0) return undefined

  const sorted = [ ...images ].sort((a1, a2) => {
    return a1.width - a2.width
  })

  for (const image of sorted) {
    if (image.width >= minWidth) return image
  }

  return sorted[sorted.length - 1]
}

export {
  durationToString,
  findAppropriateImageByWidth
}

