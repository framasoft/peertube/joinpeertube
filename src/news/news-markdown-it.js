import MarkdownIt from 'markdown-it'
import iterator from 'markdown-it-for-inline'
import replaceLink from 'markdown-it-replace-link'

function lazyLoadingPlugin(md) {
  md.renderer.rules.image = function (tokens, idx) {
    const token = tokens[idx]

    const src = token.attrs[token.attrIndex('src')][1]
    const title = token.attrIndex('title') >= 0
      ? token.attrs[token.attrIndex('title')][1]
      : ''

    const titleElem = title
      ? `<figcaption>${title}</figcaption>`
      : ''

    return '<figure>\n' +
           `  <img loading="lazy" src="${src}" title="${title}" alt="${title}" />` +
           `  ${titleElem}` +
           '</figure>\n'
  }
}

const markdownIt = MarkdownIt({
  html: true,
  linkify: true,
  breaks: true
}).use(iterator, 'url_new_win', 'link_open', function (tokens, idx) {
  // adding target="_blank"
  tokens[idx].attrSet('target', '_blank')
  tokens[idx].attrSet('rel', 'noopener noreferrer')
})
.use(lazyLoadingPlugin)
.use(replaceLink)

export {
  markdownIt
}
