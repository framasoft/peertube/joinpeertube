---
id: newsletter-2024-04
title: "April 2024 PeerTube newsletter!"
date: April 18, 2024
---

Bonjour,
We are Framasoft, the French non-profit developing the PeerTube software!

You have been waiting for this for years, and so have we...
... **welcome to the PeerTube Ecosystem Newsletter!** *jingle*

In this newsletter, we'll share with you some news, videos, projects or anything else related to the PeerTube ecosystem that we're excited about!

![Sepia popcorn](/img/news/newsletter-2024-04/2020-04-25_peertube-sketch.jpg)

#### [PeerTube in 2024](https://joinpeertube.org/news/peertube-future-2024)

PeerTube is now 6 years old 🥳!
The project has come a long way to become the mature software it is now.
Does that mean it is finished? Not at all!
Find out what we plan to do with PeerTube this year in our news from last December!
Spoiler alert: get your smartphones ready!

#### [P2Play ­— An Android PeerTube application for smartphones](https://f-droid.org/fr/packages/org.libre.agosto.p2play/)

Speaking of smartphones apps, Ivan Agosto resumed the development of P2Play!
P2Play is an open-source Android application for PeerTube.
You can download it from Play Store and F-Droid!

By the way, did you know that we at Framasoft are also [working on a mobile app](/news/peertube-future-2024)? We'll let you know as soon as we're ready!

#### [A PeerTube Retrogaming Playlist](https://social.growyourown.services/@FediVideo/112094694397523311)

Fedi.Video, a fediverse account that helps you discover PeerTube content, has made a playlist full of retrogaming content found in different instances of PeerTube!
If you like pipes and shells, this is probably for you!

#### [Obsidian Urbex - Abandonned Places Videos](https://lostpod.space/w/0643d4bd-af27-4617-a6e1-b4b4d8f826a9)

Obsidian Urbex is a channel dedicated to urban exploration videos.
If, like me, you're fascinated by abandoned places that used to be luxurious, you'll love this collection of short videos!


#### "How do I find PeerTube content?"

If you read about PeerTube on the Fediverse a lot, you'll see this question is getting asked a lot!
We, at Framasoft, are working on improving this aspect of PeerTube but for now, did you know about Sepia Search, the search engine we built to find out PeerTube content?

Check it out at https://sepiasearch.org/

#### Very quick fix

Can you believe there has been only 2 hours between the reporting of an XSS injection in embed, by Syst3m0ver from [aramido GmbH](https://aramido.de/sicherheitspruefung/penetrationstest), and the release ([v6.0.4](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.4)) with [the fix](https://github.com/Chocobozzz/PeerTube/commit/a93217d2da5d002c99528a6f0951c84c832935b9)? If you're the administrator of a PeerTube instance, please update as soon as possible to v6.0.4!

Reminder: If you find a vulnerability, please [contact us](https://github.com/Chocobozzz/PeerTube/?tab=security-ov-file#official-channels)!

---

If you enjoyed this first newsletter, please give it a thumbs up and click the bell… wait, no 🤔
But if you have any PeerTube related content (channel, videos, app…) that you want to highlight, feel free to share it with [our mastodon's account](https://framapiaf.org/@peertube) and we may relay it in our next newsletter!

Peertube also evolves thanks to your ideas, so don't hesitate to share them on [our ideas board](https://ideas.joinpeertube.org/).

À bientôt !
