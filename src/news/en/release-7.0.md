---
id: release-7.0
title: "PeerTube v7: offer a complete makeover to your video platform!"
date: december 17, 2024
---

With its brand new design, PeerTube\'s new interface isn\'t just prettier (although it is). It is also simpler, easier to use and understand and more accessible. Welcome to a new era of this software that empowers creators to get, control and connect their own video platforms.

#### Let\'s reflect the growth of the PeerTube Ecosystem

Seven year ago, PeerTube was mainly a tool that tech-savvy FOSS enthusiasts were happy to toy with. Then it became popular among content creators that wanted a self-hosted mirror of their YouTube/Twitch channels; and among communities who wanted to create and regulate their safe space (deaf people, queer people, etc.)

Nowadays, **PeerTube is experiencing increasing success** among content creators who publish original content (or exclusive content for their community), alternative media, and institutions: colleges, ministries of education, national television and radio archives, etc.

> Public structures often need to share video content without attention-grabbing mechanisms or data exploitation.

[![illustration with the PeerTube mascot and the motto \"building a free internet of the future\"](/img/news/release-7.0/en/peertube-future.jpg "Find out more about the history and values of PeerTube in this interview with the Association for Progressive Communication.")](https://www.apc.org/node/40437)

To us, this is a new step in the evolution of PeerTube\'s audiences.

That is why this year, we\'ve asked [La Coopérative des Internets](https://www.lacooperativedesinternets.fr/) to lead a thorough UX research (complete with interviews, tests, etc.) and help us start a top to bottom redesign of the interface. Our goal was to improve on PeerTube so it would better fit the need of those new audiences. We were clear that everything was on the table : colors, vocabulary, layout\...

Well, **we are proud to [release this v7 of PeerTube](https://github.com/Chocobozzz/PeerTube)**, that lays the ground to a complete remodeling of the interface.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://github.com/Chocobozzz/PeerTube">
      Check the source code
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>

#### First look: themes, new colors and vocabulary

PeerTube design, color schemes, vocabulary, etc. has been constructed over seven years, as we went along, learning, getting help from the community. This new design was an opportunity to **take a step back an get some intentions behind the interface**.

![screenshot PeerTube v7 light/beige interface](/img/news/release-7.0/en/light_beige_interface.png)

The new Light/beige default theme is calmer, easier on the eye than the original Black & orange one. We also added a Dark/brown theme into the core for the dark mode aficionados. Both aims to **facilitate video browsing**.

Creating those new themes was an opportunity to **clean up and simplify how the interface is coded** (specifically: clean up the CSS, with a focus on the variables), while limiting breakages with preexisting customized themes. It is now really **easier to create new themes for PeerTube**, and we hope you\'ll share your creations!

![screenshot PeerTube v7 dark/brown interface](/img/news/release-7.0/en/dark_brown_interface.png)

We also updated the PeerTube lingo. **There is a reason we are now using the word \"platform(s)\"** to talk about all the servers where PeerTube has been installed.

Yes, calling them \"instance(s)\" has been the norm in the tech savvy-world of activity-pub enthusiasts. But, to those not privileged enough to know about the fediverse and its protocol, the word platform is self-explanatory, and facilitates inclusion.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://joinpeertube.org">
      PeerTube Website
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>

#### Enjoying simpler and relevant layouts

There are lots of informations to display on any PeerTube page. The layouts and menus grew organically during seven years of development\... and needed some pruning! **We remodeled those menus and pages** to bring forward relevant informations, and present a more intuitive way to find out what you are looking for.

![screenshot of PeerTubev7 menus for anonymous users](/img/news/release-7.0/en/anonymous_user_interface.png)

For exemple, content creators used to access their channels and uploaded video in their library (where any PeerTube user can get to their playlists, history, etc. of the videos they watched). **Now in PeerTube v7, there is a new section called \"video space\"** specific for video uploaders\' needs.

In the same way, \"admin\" pages **for PeerTube platforms administrators** have now been separated int**o an Overview page** (to get more info about users, hosted videos, etc.), **a Moderation one** (to manage abuses, blocking, registrations), **and a Settings one** (to access configurations, runners, etc.)

![screenshot of PeerTube v7 overview page for a logged in administrator](/img/news/release-7.0/en/admin_overview.png)

The several pages that presented the videos on a PeerTube platform (Recently added, Local videos, Trending) have been merged into **a \"Browse videos\" page**, that includes quick filters to display the same selections of content in a more forward way.

The same intent has driven the **new layout of the \"Discover videos\" page** we hope it will empower curious users.

Obviously, the left bar and header menus have been reorganized to reflect those changes and make navigation even more intuitive. You can now **access your account settings and notifications from the header menu**, as it is customary on other websites.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://sepiasearch.org/">
      PeerTube Search Engine
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>

#### Displaying relevant information to show video diversity

A big feedback from new users was that the old interface was confusing, that is was hard for a user to know where they were, and where the videos came from.

That is why, in PeerTube v7, we have added **more ways for platforms owners to customize and identify their platforms**: easily add a banner (used on pages, mobile app exploration, and our search engine [SepiaSearch](https://sepiasearch.org/)) and a platform icon (used by the mobile application) More, the name and description of their platform is now displayed to non-registered users in the left hand menu.

![Screenshot of platform search results on JoinPeerTube](/img/news/release-7.0/en/platform-list-sepiasearch.jpg)

We have also changed **how video miniatures appear in all pages that lists videos**. Channel avatar are always displayed so it\'s easier to identify creators, titles are highlighted, the date and viewcount of the video are present but toned down. Those changes make pages that lists videos easier to read, and facilitate identifying the video you want to watch.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://joinpeertube.org/news/peertube-app">
      PeerTube Mobile App
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>

#### Accessibility on the forefront

The redesign was also the opportunity to **prioritize the interface accessibility** (for impaired people). [In 2023, we prepared the code and worked on what we knew](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/)\... so the planned 2024 full accessibility audit (thanks to the NGI Entrust consortium) would bring as much new and detailed improvements as possible.

Thanks to the audit, **we have improved on so many issues**: we fixed color contrats and themes, progress bar, several components, and various screen reader issues. We added missing labels on interactive elements, \"skip menu\" links, underlining to links. We also improved keyboard navigation, and re-implemented components of a non-accessible dependency.

![screenshot of the vrowse video page on peertube v7](/img/news/release-7.0/en/browse-videos-EN.jpg)

We sincerely think that **PeerTube have caught up with accessibility issues and should be up to standards**\... but we know, now, that there is always room for improvement, and for **learning directly from those who are concerned**.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://framapiaf.org/@peertube">
      Follow PeerTube on Mastodon
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>

#### Editing captions, promoting videos and more\...

With the brand new [remote transcoding tool we introduced last year](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/), getting a transcription or subtitles for your video is easier than ever. But the caption editing tool was\... hum\... let\'s say \"barebone\". We are now introducing **a new modal that makes editing captions really convenient**.

![Screenshot of PeerTube v7 new caption edition interface](/img/news/release-7.0/en/Captions-EN.jpg)

We welcomed and integrated upstream **a community contribution on SEO** (search engine optimization), to help promote PeerTube-hosted-content on search engines. A platform avatar now appears in the opengraph tags, empty accounts and channels are hidden from the sitemap, while additional video tags are now present there.

Last, **PeerTube has been translated into Slovak**.

We really want to take time to **thank the community that contributes to translations of PeerTube,** we would never have thought that our software would one day be available in more than 38 languages.

> Huge thanks to all of you, wonderful people, who took time and care to [contribute on our translation tool](https://weblate.framasoft.org/): you are amazing.

![sepia, mascotte de peertube, entretenant son jardin](/img/news/release-7.0/en/2020-05-21_Peertube-Moderation_by-David-Revoy-lowres.jpg "Illustration: David Revoy - Licence : CC-By 4.0")

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://joinpeertube.org/news">
      Subscribe to PeerTube news
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>

#### There is more to come\...

We still have **more work planned from this whole interface remodel**. We hope to deliver it in the firsts months of 2025. First, we are currently finishing the translation of **the UX research report** from La Coopérative des Internets and **we will publish it** in the hope that it will help the whole fediverse community.

We will also wait a bit for PeerTube administrators to update their platforms, and then **update the PeerTube documentation** with new screenshots, and the new menus pathways.

Our next interface changes will focus on **streamlining the channels & videos management experience** for content creators (where several tools and menus added to the pile over the years). We also plan on **fine-tuning the categorization of NSFW videos**.

[![Illustration - Dans la mer Sepia, læ poulpe mascotte de PeerTube, dessine un grand chiffre sept avec son encre.](/img/news/release-7.0/en/PeerTube-v7-CC-BY-David-Revoy.jpeg "Illustration: David Revoy - Licence : CC-By 4.0")](https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN)

We obviously have many more items to **our 2025 roadmap, but are still trying to secure funds to realize them**: we\'ll keep you informed as soon as we know more!

About funds, we really want to **thank the [NGI0 Entrust](https://nlnet.nl/entrust/) program** for their grant that funded most of the work on this new version (and on the PeerTube mobile app we released last week). **The [NLnet](https://nlnet.nl/) team has been a great partner** on the management of this grant, and **the accessibility audit from the consortium** has really made a huge difference.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://ideas.joinpeertube.org/">
      Share ideas & Feedback on PeerTube
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7EN">
      Support Framasoft
   </a>
</div>
