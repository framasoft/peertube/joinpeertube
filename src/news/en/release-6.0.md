---
id: release-6.0
title: "PeerTube v6 is out, and powered by your ideas !"
date: November 28, 2023
---

It's #givingtuesday, so we're giving you PeerTube v6 today! [PeerTube](https://joinpeertube.org) is the software we develop for creators, media, institutions, educators... to manage their own video platform, as an alternative to YouTube and Twitch.

<div class="news-card">
<p>
   <strong>🦆 VS 😈: Let's take back some ground from the tech giants!</strong>
</p>

<em>Thanks to <a href="https://support.joinpeertube.org">your donations to our not-for-profit</a>, Framasoft is taking action to advance the ethical, user-friendly web. Find a summary of our progress in 2023 on our <a href="https://support.joinpeertube.org">Support Framasoft page</a>.</em>

➡️ <a target="_blank" rel="nofollow noreferrer noopener" href="https://framablog.org/tag/collectivise-internet-convivialise-internet/">Read the series of articles from this campaign</a> (Nov. - Dec. 2023)
</div>

The sixth major version is being released today and we are very proud! It is the most ambitious one since we added peer-to-peer livestreaming. There is a good reason for that: we packed this v6 with features inspired by [your ideas](https://ideas.joinpeertube.org)!

We are so eager to present all the work we achieved that we'll get right into it. But stay tuned: in two weeks, we'll take more time to talk about PeerTube's history, the state of this project and the great plans we have for its future!

![](/img/news/release-6.0/en/3-youtube-videoraptor-light-265x300.png)

#### This year: two minor updates and a major achievement

In 2023, and before preparing this major update, we released only two minor versions... but one of them brought to the table a major technical feature that will help democratize video hosting even more.

##### March 2023: PeerTube v5.1

You'll get more details in [the news dedicated to the 5.1 release](https://joinpeertube.org/news/release-5.1), so to keep it short, this version brought:

   * An "asking for an account" feature, where instance moderators can **manage and moderate news account requests**
   * A **back-to-live button**, so when you can lag behind during a livestream, you can go back to the direct
   * improvements on the **authentification plugin**, to facilitate signing on with external credentials

##### June 2023: PeerTube 5.2...

As you'll find out in our [5.2 release blogpost](https://joinpeertube.org/news/release-5.2), there were some smaller but important new features such as:

   * Adapting **RSS feeds to podcast standards**, so any podcast client could be able to read a PeerTube channel, for example
   * The option to **set the privacy of a livestream replay**, that way streamers can choose beforehand if the replay of their live will be *Public*, *Unlisted*, *Private* or *Internal*
   * Improved mouse-free navigation: for those who prefer or need to **navigate using their keyboard**
   * And **upgrades in our documentation** (it's quite thorough: [check it out](https://docs.joinpeertube.org/)!)

##### ...with a major feature: Remote Transcoding

But the game changer in this 5.2 release was the [new remote transcoding](https://docs.joinpeertube.org/admin/remote-runners) feature.

When a creator uploads a video (or when they are streaming live), PeerTube needs to transform their video file into an efficient format. This task is called video transcoding, and it consumes lots of CPU power. PeerTube admins used  to need (costly) big-CPU servers for a task that wasn't permanent... until remote transcoding.

Remote transcoding allows PeerTube admins to deport some or all of their transcoding tasks to another, more powerful server, one that can be shared with other admins, for example.

**It makes the whole PeerTube administration cheaper, more resilient, more power-efficient**... and opens a way of sharing resources between communities!

We want, once again to thank the NGI Entrust program and the NLnet foundation for the grant that helped us achieve such a technical improvement!

![](/img/news/release-6.0/en/3-sepia-276x300.png)

#### PeerTube v6: powered by your ideas!

Enough about the past, let's detail the features of this new major version. Note that, for this whole 2023 roadmap, we developed features suggested and upvoted by... you! Or at least by those of you who shared your ideas on [our feedback website](https://ideas.joinpeertube.org).

##### Protect your videos with passwords!

That was a very awaited feature. Password-protected videos can be used in lots of situations: to create exclusive content, mark a step in a pedagogical plan, share videos with people trusted by the ones you trust...

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/e15b5e51-d603-41f4-b911-dcd88a651bc2?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

On their PeerTube account, creators can now set a single password when they upload, import or update the settings of their videos.

But with our REST API, admins and developers can take it a step further. They can set and store as many passwords as they want, thus easily give and revoke access to videos.

This feature was the work of Wicklow, during his internship with us.


##### Video storyboard: preview what's coming!

If you like to peruse your videos online, you might be used to hover the progress bar with your mouse or finger. Usually, a preview of the frame appears as a thumbnail: that's called a storyboard feature, and that's now available in PeerTube!

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : Storyboard" src="https://framatube.org/videos/embed/73556243-7a9b-496c-a740-f80e42ee0ad9?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

Please note that as Storyboards are only generated when uploading (or importing) a video, they will only be available for new videos of instances that upgraded to v6...

Or you can ask, very kindly, to your admin(s) that they use the magical `npm run create-generate-storyboard-job` command (warning: this task might need some CPU power), and generate storyboards for older videos.


##### Upload a new version of your video

Sometimes, video creators want to update a video, to correct a mistake, offer new informations... or just to propose a better cut of their work!

Now, with PeerTube, they can upload and replace an older version of their video. Though the older video file will be permanently erased (no backsies !), creators will keep the same URL, title and infos, comments, stats, etc.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : reupload video" src="https://framatube.org/videos/embed/aeb01797-5adf-4297-90dc-c927c63eef08?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

Obviously, such a feature requires trust between videomakers and admins, who don't want to be responsible for a cute kitten video being "updated" into an awful advertisement for cat-hating groups.

That's why such a feature will only be available if admins choose to enable it on their PeerTube platforms, and will display a "Video re-upload" tag on updated videos.


##### Get chapters in your videos!

Creators can now add chapters to their videos on PeerTube. In a video settings page, they'll get a new "chapters" tab where they'll only need to specify the timecode and title of each chapter for PeerTube to add it.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : chapters" src="https://framatube.org/videos/embed/6f0feeeb-cade-47d8-bfbf-a9a8504efdf3?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

If they import their video from another platform (*cough* YouTube *cough*), PeerTube should automatically recognize and import chapters set on this distant video.

When chapters are set, markers will appear and segment the progress bar. Chapter titles will be displayed when you hover or touch one of those chapters segments.


##### Stress tests, performance and config recommandations

Last year, thanks to French indie journalist David Dufresne's Au Poste! livestream show and his hoster Octopuce, we got a livestream stress test with more than 400 simultaneous viewers: [see the report here on Octopuce's blog [FR]](https://www.octopuce.fr/test-de-charge-dun-peertube-en-live-avec-auposte/).

Such tests are really helpful to understand where we can improve PeerTube to reduce bottlenecks, improve performance, and give advice on the best configuration for a PeerTube server if an admin plans on getting a lot of traffic.

That's why this year, we have decided to realize more tests, with a thousand simultaneous users simulated both in livestream and classic video streaming conditions. Lots of thanks and datalove to Octopuce for helping us deploy our test infrastructure.

We will soon publish a report with our conclusions and recommended server configurations depending on usecases (late 2023, early 2024). In the meantime, early tests motivated us to **add many performances improvements** into this v6, such as (brace yourselves for the technical terms):
* Process unicast HTTP job in worker threads
* Sign ActivityPub requests in worker threads
* Optimize recommended videos HTTP request
* Optimize videos SQL queries when filtering on lives or tags
* Optimize /videos/{id}/views endpoint with many viewers
* Add ability to disable PeerTube HTTP logs


##### ...and there's always more!

A new major version always comes with its lot of changes, improvements, bugfixes, etc. You can read [the complete log here](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.0), but here are the highlights:

* We needed to settle a technical debt: **v6 removes support for WebTorrent to focus on HLS (with WebRTC P2P)**. Both are technical bricks used to get peer-to-peer streaming in web browsers, but HLS is more fitted to what we are doing (and plan to do) with PeerTube
* The video player is more efficient
  * It is not being rebuilt anymore every time the video changes
  * It keeps your watching settings (speed, fullscreen, etc.) when the video changes
  * It automatically adjust its size to match the video ratio
* We have improved SEO, to help videos hosted on a PeerTube platform appear higher in the search results of search engines
* We worked a lot on improving PeerTube's accessibility on many levels, to streamline the experience of people with disabilities.

![](/img/news/release-6.0/en/5-youtube-premium-yetube-light-279x300.png)

#### What about PeerTube's future?

With YouTube waging war against adblockers, Twitch increasingly exploiting streamers, and everyone becoming more and more aware of the toxicity of this system... PeerTube is getting traction, recognition and a growing community.

We have so many announcements to make about the future we plan for PeerTube, that we will publish a separate news, in two weeks. We are also planning on hosting an "Ask Us Anything" livestream, to answer the questions you'd have about PeerTube.

Please stay tuned by subscribing to [PeerTube's Newsletter](https://joinpeertube.org/news), following [PeerTube's Mastodon account](https://framapiaf.org/@peertube) or keeping an eye on the [Framablog](https://framablog.org).

[![](/img/news/release-6.0/en/sepia-1024x576.jpg "Click to support us and help Sepia push back Videoraptor – Illustration CC-By David Revoy")](https://support.joinpeertube.org/en/)


#### Thank you for supporting PeerTube and Framasoft

In the meantime, we want to remind you that all these developments were achieved by only one full-time payed developer, an intern, and a fabulous community (lots of datalove to Chocobozzz, Wicklow, and the many, many contributors: y'all are amazing!)

Framasoft being a French not-for-profit mainly funded by grassroots donations (75% of our yearly income comes from people like you and us), PeerTube development has been funded by two main sources:

   * French-speaking FOSS enthusiasts
   * Grants from the NGI initiative, through NLnet (in 2021 & 2023)

If you are a non-French-speaking PeerTube aficionado, please consider **supporting our work by [making a donation to Framasoft](https://support.joinpeertube.org)**. It will greatly help us fund our many, many projects, and balance our 2024 budget.

Once again this year we need you, your support, your sharing to help us regain ground on the toxic GAFAM web and multiply the number of ethical digital spaces. So we've asked [David Revoy](https://www.peppercarrot.com/fr/files/framasoft.html) to help us present this on our [support Framasoft](https://support.joinpeertube.org) page, which we invite you to visit (because it's beautiful) and above all to share as widely as possible:

[![Screenshot of the Framasoft 2023 donation bar at 12% - €23575](/img/news/release-6.0/en/donation.png)](https://support.joinpeertube.org)

**If we are to balance our budget for 2024, we have five weeks to raise €176,425: we can't do it without your help!**

<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" rel="nofollow noreferrer noopener" href="https://support.joinpeertube.org/">
      Support Framasoft
   </a>
</div>

Thanks again for supporting PeerTube,
Framasoft's team.
