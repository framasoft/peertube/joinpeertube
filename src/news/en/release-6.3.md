---
id: release-6.3
title: "PeerTube v6.3 released!"
date: September 18, 2024
---

This is the last minor release before v7, but it's packed with interesting new features! Let's have a look :)

#### Separate audio and video streams for more flexibility

Separating the audio and video streams not only reduces the size of the files on the server, as it is no longer necessary to duplicate the audio file for each video resolution, but also allows you to stream only the audio of a video via an *Audio only* option in the player!

This is a very useful feature for streaming music that can display an album cover, or for using less bandwidth when listening to a conference where no video is required.

![audio only animated image](/img/news/release-6.3/en/audio_only_EN.gif)

This new feature also allows PeerTube to ingest an audio only live stream. This means that you can stream live music on PeerTube, which will only display an *Audio* player.

Finally, we've taken the opportunity to simplify the video download modal, which now displays the essential information for each available resolution, with the option to include or exclude audio in the file being downloaded. The advanced options are still available by selecting *Video File* next to the *Download* modal title.

![download modal](/img/news/release-6.3/en/download_modal_EN.png)


#### Browse subtitles in the transcription widget

In addition to the classic subtitles integrated into the video player, it is now possible to have a subtitle panel on the right side of the video. This panel allows you to:

  * Follow subtitles in real time
  * Return to a section of the video by clicking on a sentence
  * Search for a sentence or word to return to a specific point in the video

![transcription widget](/img/news/release-6.3/en/peertube_transcription_widget_search.png)

#### Set up Youtube-dl for smoother imports

Youtube-dl is an essential tool for importing videos and related information from other video platforms (such as PeerTube, Youtube, Vimeo and [many others](https://ytdl-org.github.io/youtube-dl/supportedsites.html)).

This new version of PeerTube adds the ability for administrators to set up multiple proxies dedicated to youtube-dl, which PeerTube will randomly select to bypass certain restrictions. It is also now possible to use another version of youtube-dl, a binary that contains additional dependencies that enable features such as impersonation (i.e. pretending to be a real browser).


#### And much more

As with every release, a number of user experience and interface improvements have been made:

  * Better resolution label for custom video aspect: *1920x816* videos are now displayed as *1080p* instead of *816p*
  * Better visibility of chapter markers in the player's progress bar: they are now displayed as small dots in the progress bar
  * Smoother resume of live playback: only the video player is reloaded
  * Added the ability to easily copy server error logs via a dedicated button
  * Instance admins can now change the maximum number of frames per second for videos (limited to 60 by default)

You can find all the bug fixes, improvements and other changes in [the changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.3.0)!

Finally, we're still [preparing the mobile application](https://joinpeertube.org/news/peertube-future-2024) and we'll have some good news to announce at the end of the year!

---

You can help us continue to improve PeerTube by sharing this information, [suggesting improvements](https://ideas.joinpeertube.org/) and, if you can afford it, making [a donation to Framasoft](https://support.joinpeertube.org/), the association that develops PeerTube.

Thanks in advance for your support!
Framasoft

