---
id: peertube-future-2024
title: Mobile App, redesign, new dev, promotion... let’s build a bright future for PeerTube!
date: December 12, 2023
image: https://framablog.org/wp-content/uploads/2023/11/sepia-1024x576.jpg
---

Developing an ethical and emancipating alternative to YouTube, Twitch or Vimeo without Surveillance Capitalism’s means is a huge undertaking. Especially for a small French not-for-profit that already manages several projects to promote digital commons.

<div class="news-card">
<p>
   <strong>🦆 VS 😈: Let's take back some ground from the tech giants!</strong>
</p>

<em>Thanks to <a href="https://support.joinpeertube.org">your donations to our not-for-profit</a>, Framasoft is taking action to advance the ethical, user-friendly web. Find a summary of our progress in 2023 on our <a href="https://support.joinpeertube.org">Support Framasoft page</a>.</em>

➡️ <a target="_blank" rel="nofollow noreferrer noopener" href="https://framablog.org/tag/collectivise-internet-convivialise-internet/">Read the series of articles from this campaign</a> (Nov. - Dec. 2023)
</div>

We (Bonjour! We are Framasoft!) have been developing PeerTube for six years. Two weeks after [releasing the sixth version of the software](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/), let’s take a step back on six years of work, examine the huge opportunity that the present times hold for PeerTube, and look towards what we plan to do next year to prepare for its success... if you give us [the means to get there](https://support.framasoft.org)!

[![](https://framablog.org/wp-content/uploads/2023/11/5-youtube-premium-yetube-light-279x300.png "Click to support Framasoft and push back against the Yetube – Illustration CC-By David Revoy")](https://support.framasoft.org)

#### Not a rival, just an alternative

The realization that led us to develop PeerTube is that no one can rival YouTube or Twitch. You would need Google’s money, Amazon servers’ farms... Above all, **you would need the greed to exploit millions of creators** and videomakers, groom them into formatting their content to your needs, and feed them the crumbs of **the wealth you gain by farming their audience into data livestock**.

Monopolistic centralized video platforms can only be sustained by surveillance capitalism.

We wanted small groups such as institutions, educators, communities, artists, citizens, etc. to be able to afford to emancipate themselves from Big Tech’s platforms, without getting lost in the world wide web. We needed to develop a tool to democratize videohosting, so it had to be designed with radically different values in mind.

**And that is what we did. We build PeerTube to empower people, not databases or shareholders.**

Today, PeerTube is:

*   a **Free-Libre software** (transparency, protection against monopoly)
*   you can host **on your server** (self-hosting, autonomy, empowerment)
*   to create your video and livestream platform, **with your own rules** (community building, self-management)
*   that lets you **federate** (or not!) to other PeerTube platforms through ActivityPub protocol (federation, network, outreach)
*   that adds (optional) **peer-to-peer streaming** to classic streaming so it can withstand affluence (resilience, sharing, decentralization)
*   where more powerful servers can help less fortunate ones with **redundancy** (solidarity, resilience)
*   that can **store videos externally** with S3 storage (adaptability, cost-efficiency)
*   that can **deport CPU-hungry tasks** such as video or live transcoding to a dedicated server (efficiency, resilience, sustainability)

So no: PeerTube is not, and will not be a rival to YouTube or Twitch. **PeerTube is powered by other values that those coded into Google’s and Amazon’s ecosystems**. PeerTube is an alternative, and that’s exactly why this is so exciting.

[![](https://framablog.org/wp-content/uploads/2023/11/3-sepia-276x300.png "Click to support Sepia – illustration David Revoy – licence: CC-By 4.0")](https://support.framasoft.org)

#### PeerTube is a software: 6 years of developments

In the last six years, with more than 275 000 lines of code, we got:

*   From a POC to a fully operative federated video platform with p2p broadcasting, complete with subtitles, redundancy, video import, search tools and localization ([PeerTube v1, oct. 2018](https://framablog.org/2018/10/15/peertube-1-0-the-free-libre-and-federated-video-platform))
*   Notifications, playlists, a plugin system, moderation tools, federation tools, a better video player, a presentation website and an instances index ([PeerTube v2, nov. 2019](https://framablog.org/2019/11/12/peertube-has-worked-twice-as-hard-to-free-your-videos-from-youtube/))
*   Federated research tool (and a search engine https://sepiasearch.org), more moderation tools, lots of code improvement, UX revamping, and last but not least: p2p livestream ([PeerTube v3, Jan. 2021](https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive/))
*   Improved transcoding, channels and instances homepage customization, improved search, an even better video player, filtering videos on pages, advanced administration and moderation tools, new video management tool, and a big code cleaning session ([PeerTube v4, Dec. 2021](https://framablog.org/2021/11/30/peertube-v4-more-power-to-help-you-present-your-videos/))
*   A video editing tool, improved video statistics and metrics display, replay feature for permanent livestreams, latency settings for lives, an improved video player (for mobile displays), a more powerful plugin system, more customization options, more video filtering options, a new and user friendly feedback tool and a renewed presentation website ([PeerTube v5, Dec. 2022](https://framablog.org/2022/12/13/peertube-v5-the-result-of-5-years-handcrafting/))
*   Account request moderation, « back to live » button, remote transcoding (to deport CPU hungry task on a dedicated server). storyboard (previews in the progress bar), video chapters, improved accessibility, upload a new version of a video, and password-protected videos. ([PeerTube v6, Nov. 2023](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/))

And that is just **when you only consider the software development part of PeerTube**. In order to support and promote this software, we had to build a whole ecosystem.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : chapters" src="https://framatube.org/videos/embed/6f0feeeb-cade-47d8-bfbf-a9a8504efdf3?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

#### PeerTube is also an ecosystem

PeerTube, nowadays, is **also a coding community**. On the [project forge](https://github.com/Chocobozzz/PeerTube/) (online space to contribute on developments), we’ve had more than 400 contributors, 4,300 issues (features and support requests) closed over 6 years and 500 still open, and 12,400 contributions integrated upstream.

As not anyone can familiarize themselves with more than 275 000 lines of code, an easy way to **contribute to PeerTube is by developing plugins**: there are hundreds of them! Among them, there are the live chat (to get a chat during livestreams), plugins to authenticate against external authentication platforms, annotations to add in the video player, a transcription plugin to automatically create subtitles for your videos or plugins to add monetization to PeerTube videos.

Contributors also helped by **translating PeerTube** into more than 36 languages ([join them here](https://weblate.framasoft.org/projects/peertube/#languages)), by providing answers [on our forum](https://framacolibri.org/c/peertube/38), by updating our [official documentation](https://docs.joinpeertube.org/), or by sharing ideas on our [Let’s Improve PeerTube feedback tool](https://ideas.joinpeertube.org/).

There are now more than a thousand PeerTube platforms all over the world (that we know of ^^), hosting almost a million videos. We created [an instances index](https://instances.joinpeertube.org/instances) that feeds content to [SepiaSearch](https://sepiasearch.org), our **search engine for PeerTube** videos, channels and playlists. We moderate it according to our terms and conditions, but anyone is free to use the code we develop to create their own [index](https://framagit.org/framasoft/peertube/instances-peertube) and [search engine](https://framagit.org/framasoft/peertube/search-index).

Fortunately, others are working towards **promoting and moderating PeerTube content**, by creating [directories \[FR\]](https://peertube-annuaire.nogafam.fr/), [recommendations threads](https://social.growyourown.services/@FediFollows/111291322079656821), [moderation tools](https://peertube_isolation.frama.io/), [Firefox extensions](https://addons.mozilla.org/fr/firefox/addon/peertube-companion/), and all kinds of amazing content.

We **promote PeerTube** with an official website [Joinpeertube.org](https://joinpeertube.org), where the latest news are shared on the [blog and the newsletter](https://joinpeertube.org/news). There is also a [mastodon account](https://framapiaf.org/deck/@peertube) (and an -almost abandoned- [account on Twitter](https://twitter.com/joinpeertube)). We also spend lots of hours talking to medias, researchers, innovators, communities, contributors, etc.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : Storyboard" src="https://framatube.org/videos/embed/73556243-7a9b-496c-a740-f80e42ee0ad9?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

#### Fighting dragons with toothpicks

So, how can we **estimate the cost of those 6 years of work**? Should we just consider development time and the management of the development community (issues, code review, support)?

Should we also count the work done on blogposts, illustrations and promotion material, establishing roadmaps, working with designers, exchanging experience with researchers, videomakers, and [amazing projects,](https://bunseed.org/) some of which [we supported with funds](https://github.com/JohnXLivingston/peertube-plugin-livechat/releases/tag/v8.0.0)? What about the time for moderating our [search engine](https://sepiasearch.org) or cleaning after spammers on our [feedback tool](https://ideas.joinpeertube.org/)?

Even though we cannot pinpoint the exact budget Framasoft spent on PeerTube since 2017, **our conservative estimate would be around 500 000 €**. Over six years. As we got two grants from the European commission (through the NGI0 [Search & Discovery](https://nlnet.nl/project/PeerTube/) and [Entrust](https://nlnet.nl/project/Peertube-Transcode/) programs) totaling 132 000 €, it means that 73,6 % of PeerTube budget came from donations.

Now **let’s overestimate the cost of PeerTube to 600 000 €** over 6 years, to make sure we covered every expense.

**Even then, PeerTube total cost would represent 22 millionth (0.0022 %) of YouTube’s ad revenues last year. Yes, we did the math.**

_([source](https://www.statista.com/statistics/289658/youtube-global-net-advertising-revenues/) – 29.243 B USD // 632,853 USD)_

We are – figuratively – fighting dragons with toothpicks. That’s why we think that PeerTube cannot and will not rival YouTube nor Twitch (and even less TikTok that presents a whole other experience).

But, as an alternative, PeerTube is already successful.

[![](https://framablog.org/wp-content/uploads/2023/11/sepia-1024x576.jpg "Click to support Sepia against the Videoraptor – illustration David Revoy – licence: CC-By 4.0")](https://support.framasoft.org)


#### A success in our eyes

Today, we know of more than 1000 instances (servers on which PeerTube is installed and running), sharing almost a million videos.

As it is not limited by the [captology](https://en.wikipedia.org/wiki/Captology) mechanics of an ad-and-attention-based model, **PeerTube offers features not available from tech giants**:

*   **compatibility with other social tools** via [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) (Imagine you could tweet a comment to a YouTube video: with Mastodon and PeerTube, you can.)
*   **share a video** from a start timecode **to a stop timecode** (YouTube has caught up with us, since)
*   **untempered chronological access to your suscriptions** feed (no need to « click the bell » in addition to subscribing)
*   **password-protected videos** (unavailable in YouTube, paid in Vimeo)
*   **replace a video** by an updated version

We intended to make PeerTube specifically for people that need (and want) to s**hare their videos outside of the surveillance capitalism model**. Obviously we all know (and like) some YouTubers and Twitch-streamers, but they are the visible part of the iceberg of online video sharing.

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/aeb01797-5adf-4297-90dc-c927c63eef08?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>


Institutions, Educators, Independent medias, Citizens, and even creators should have the freedom to share videos online without contributing to a company’s monopoly, having to accept forced advertisement, or sacrificing on their audience’s data and privacy. The great news is, some of them have already found such freedom, and it makes us proud:

*   **Institutions**
    *   [European commission](https://tube.network.europa.eu)
    *   [Netherlands Institute for sound and vision](https://peertube.beeldengeluid.nl)
*   **Educators**
    *   French Ministry of Education [FOSS-based platform](https://tubes.apps.education.fr)
    *   University of Philippines Diliman’s [digital learning programs](https://stream.ilc.upd.edu.ph)
*   **Independent medias**
    *   [Blast](https://video.blast-info.fr) (French independent left-wing online media)
    *   [Howlround](https://howlround.com) (Theater Commons media based in the Emerson College, Boston)
*   **Citizens**
    *   [Urbanists.video](https://urbanists.video) (videos about walkable, livable places)
    *   S2S (safe space for deaf and hearing impaired people, videos about/in French Sign Language)
    *   [Live it live](https://liveitlive.show) (live music concerts)
*   **Creators**
    *   [Skeptikon](https://skeptikon.fr) (French collective, videos about critical thinking and scepticism)
    *   [TILvids](https://tilvids.com) (Til = Today I Learned, edutainment videos in English, with authorized and official YouTube mirroring)
    *   [Bunseed](https://bunseed.org) (French initiative, FOSS-based alternative to Patreon, by and for creators, built upon PeerTube)

We want to build on the recognition PeerTube is getting, that’s why we have planned a lot of work for 2024!

<div class="mt-3 mb-3" style="margin: auto;">
  <div style="position: relative; padding-top: 56.25%;">
    <iframe title="Peertube presentation at NGI forum 2023 - by Pouhiou" width="100%" height="100%" src="https://framatube.org/videos/embed/5ddc8a25-33be-4a93-b710-bef1b6145d4e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;"></iframe>
  </div>
</div>

#### PeerTube’s roadmap for v7, in 2024

The features we have planned for the next year of development on PeerTube all have the same goal: facilite adoption by improving ease-of-use in several ways. As for version 6, most of those features has been chosen from the ideas you shared and voted for [on our feedback tool](https://ideas.joinpeertube.org/).

We plan to:

*   **Add a data export/import system** (with or without video files), so users can easily change their instance.
*   Get **a full accessibility audit**, to facilitate use for people with specific needs, and complete the work done this year (see version 6 release). If we have time left on integrating the report’s recommandations, we will see if and how we could add speech-to-text transcription
*   Add a **comment moderation tool** usable for both instance administrators and video uploaders.
*   Create **a new moderation tool** to sort content according to preset keywords lists ( « far-right dogwhistling words in German », « queerphobic idioms in English », etc). This tool will present corresponding content to instance administrators and moderators, that will then determine if it fits their moderation policy.
*   (Technical) **separation of audio and video streams**. Such improvement will unlock the possibility, in the future, to develop and get multi-audio track videos (e.g. multiple langages), or multi-videos track with the same audio stream (e.g. multiple angles)
*   Add **a new « audio-only » resolution** (in the « 720p », « 1080p », etc. menu) for our HLS player. It will enable users to only get the audio track streamed to them, improving sustainability when they only want to listen to a video and look at other tabs.
*   Rethink the **sensitive content characterization**. At the moment, you can only tag videos as « Safe For Work » / « Not Safe For Work ». But « sensitive content » can imply lots of cases: violence, nudity, strong langage, etc. We will work with designers to think about the appropriate way to characterize and treat such cases.
*   **Revamp the video management space**. We have added lots of new features along the years (live and replay, studio editor, etc.)... it’s great, but tabs and menus accumulated. We will work with designers to rethink it from the ground up and make it easy-to-use.
*   Get a complete review and implement a **redesign of the experience and interface of PeerTube**. Even though we’ve had lots of help along the way, PeerTube has not benefited of guidance in design from the get-go. We want to think this work as a reboot, where everything (even the orange?) is on the table, if it helps with adoption and ease of use.

[![](https://framablog.org/wp-content/uploads/2023/11/3-youtube-videoraptor-light-265x300.png "Hep us push back against the Videoraptor- Illustration CC-By David Revoy")](https://support.framasoft.org)


#### Doubling the dev team for resilience...

OK, when you go from one to two developers, « doubling » is easier... but it was still a big deal to us.

First, because **Framasoft is a not-for-profit [funded mainly by donations](https://support.framasoft.org)**. So far, we’ve had the honor and privilege to get enough support to fund our expenses, the main being our 10 employees. But donation-based economics models are, by definition, highly unpredictable. That is especially true in an economy where inflation, energy costs, etc. make most of our supporters rethink their budget.

Another reason lies within [our core value](https://framasoft.org/en/manifest): **we believe in decentralization and networks of small actors** (over growing into giants and monopolies). We also believe that prioritizing humans and care implies to **stay in a small team configuration**, where we truly know each other.

And we think that the way we applied those values into our not-for-profit is key to the efficiency, the creativity and the talents expressed by our members (both volunteers and employees). That’s why we worked on limiting Framasoft’s growth, and had the symbolic limit of « ten employees tops ».

During 2022 and 2023, there were lots of discussions on this topic within Framasoft. On one hand, we can’t keep on developing PeerTube with only one developer (even though someone as talented as Chocobozzz), who could win the lottery, leave, or just change carriers. On the other hand, if we hired a new developer, what would be their profile? How can we make sure they would fit in? Can we secure a long lasting job for them?

In **late 2022, Chocobozzz asked us to post an internship offer**. It was both to test if, after 5 years coding solo on PeerTube, teamwork came back easily (it did) ; but also to train someone on PeerTube’s code core, see how it can be apprehended by newcomers, and how to improve its documentation.

Wicklow joined us for an internship between February and August 2023, and produced the « password protected video features » released in [version 6 of PeerTube](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/). **We didn’t plan on hiring him**: we had, then, other profiles in mind, and thought we wouldn’t be able to start a hiring process before 2024. We specifically told him so, as not to give him false hope... But as we benefited from a [grant extension from NGI0 program](https://nlnet.nl/project/Peertube-Transcode/), we also realized that he was a perfect fit in the project, for the team and in our not-for-profit.

**Long story short: we hired Wicklow in September 2023**, just as he graduated, on a one-year contract (that we hope to secure [with your help!](https://support.framasoft.org)).

<div class="mt-3 mb-3" style="margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe loading="lazy" width="100%" height="100%" title="Peertube V6 : password protection" src="https://framatube.org/videos/embed/e15b5e51-d603-41f4-b911-dcd88a651bc2?loop=1&autoplay=1&muted=1" frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;" allowfullscreen="allowfullscreen"></iframe>
   </div>
</div>

#### ...and to create an iOS/Android mobile app!

This new hire has two goals. First and foremost, we want another developer to become familiar with PeerTube’s core code, and **lessen the « [bus factor](https://en.wikipedia.org/wiki/Bus_factor) »** . Wicklow should also become gradually able to help Chocobozzz in managing the code community.

As the community grows (and we are very thankful), so does the managing workload: answering to [issues](https://github.com/Chocobozzz/PeerTube/issues/) and support requests [on our forum](https://framacolibri.org/c/peertube/38), reviewing code contributions, etc. Even though being present for the community is important, it’s taking up to half of Chocobozzz’s time, and that means even less time to develop new features.

The second and main goal for Wicklow in 2024 would be, with the help of designers, **to create and publish an official PeerTube mobile app**. Mobile viewing has become the main way to watch videos. Even though [there are already mobile apps that can play videos on PeerTube](https://docs.joinpeertube.org/use/third-party-application), we feel that an official app could help with PeerTube’s adoption and attractiveness.

For 2024, the app would be limited to finding and watching videos. We want users to be able to use a federated search engine, watch videos and livestreams, log in to their account on their PeerTube instance, access their notifications, subscriptions, playlists, etc. If successful, this first version of the app could be extended to other use-cases and features in the future.

Our plan is **to publish this app both on iOS** (pending Apple’s review, that [can be tricky](https://laurenshof.online/owncast-and-the-app-store/)) **and Android**... and, as an extended goal (so « if all goes well »), on Android TV as well.

[![](https://framablog.org/wp-content/uploads/2023/11/5-super-sepia-196x300.png "Sepia, PeerTube’s mascot, strong from your support – illustration David Revoy – licence: CC-By 4.0")](https://support.framasoft.org)

#### Promoting the PeerTube Ecosystem

PeerTube is more than code, and we want to **shed a light on the incredible community that is thriving around this project**.

We often see amazing plugins, interesting instances and channels, new initiatives and experimentations... that we would like to share. But we seldom have and take time to do so.

In the meantime, we also witness many people wondering if PeerTube allows livestream (it does!) if there is a chat for lives (yes: it’s a great plugin!), or if there are websites to find content on PeerTube (yes again!)

We plan to **work on promoting PeerTube’s ecosystem**, through the [blog and newsletter](https://joinpeertube.org/news) on our website [JoinPeerTube](https://joinpeertube.org), our [Mastodon account](https://framapiaf.org/@peertube), and by working on a showcase [Peer.tube](https://peer.tube) instance.

To kick off this work, **we will go live and answer all your questions about PeerTube** during a livestream hosted by Laurens from the [Fediverse Report blog and newsletter](https://fediversereport.com/), on our [Peer.Tube channel](https://peer.tube/c/peertube_news/videos)! You can already go on Mastodon and ask your questions with the #PeerTubeAMA hashtag.

This AMA (« Ask Me Anything ») will take place tomorrow, Dec 13th, from 6 to 8pm (CET), [on this link](https://peer.tube/w/f6jxvT1WZzsBRHJF6t6saD).

[![Thumbnail stating "Livestream #PeerTubeAMA - Dec. 13th - 6-8pm CET](https://framablog.org/wp-content/uploads/2023/12/PeerTube_AMA6-1024x709.png "Click on the image to get to the livestream")](https://peer.tube/w/f6jxvT1WZzsBRHJF6t6saD)

(and if all goes well, we’ll publish the replay [on the same channel](https://peer.tube/c/peertube_news/videos))

#### Funded by you... and Europa!

As we stated sooner in this (long) blogpost, we were fortunate enough to get grants from the European Commission program NGI, through the [NLnet foundation](https://nlnet.nl/) (many thanks to them!). The previous grants helped us fund a quarter of our six years of work on PeerTube. We are glad to announce that we got [another grant for 2024](https://nlnet.nl/project/PeerTube-mobile/), that will cover planned development costs.

It means that, as it was for 75 % of the work until now, funding the rest of our plans relies on donations. Communicating about PeerTube and its ecosystem, sharing experience with diverse actors, design costs, community support and management, etc. All those costs will be, as usual, [funded by... some of you](https://support.framasoft.org)!

Our current donation campaign will determine Framasoft budget for 2024, and from its success we will know if we can secure a stable job for our second developer, while keep on [all the other projects and actions that we take on](https://framablog.org/tag/collectivise-internet-convivialise-internet/).

Once again this year we need you, your support, your sharing, to help us regain ground on the toxic GAFAM web and multiply ethical digital spaces.

So we’ve asked [David Revoy](https://www.peppercarrot.com/fr/files/framasoft.html) to help us present this on our « [Support Framasoft](https://support.framasoft.org/) » page, which we invite you to visit (because it’s beautiful) and above all to share as widely as possible:

[![Framasoft donation bar on dec. 12th 2023, at 30 % - 61341 €](https://framablog.org/wp-content/uploads/2023/12/2023-12-12-Soutenir-Framasoft-1024x545.png)](https://support.framasoft.org)

**If we are to balance our budget for 2024, we have three weeks to raise €138,659: we can’t do it without your help!**

<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" rel="nofollow noreferrer noopener" href="https://support.joinpeertube.org/">
      Support Framasoft
   </a>
</div>
