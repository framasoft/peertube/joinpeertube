---
id: roadmap-v6
title: What 2023 will bring for PeerTube...
date: February 8, 2023
---

We (Framasoft, a small not-for-profit association!) are proud to present our roadmap for **the developments and side projects we have planned for PeerTube in 2023**. This roadmap combines both the progress we wanted to make to the software and the proposals you have sent us [over the last few months](https://ideas.joinpeertube.org/).

It is important to note that we are dedicating only one developer to PeerTube (yes, only one!), PeerTube being one of more than fifty projects led by our association.

#### End of February: PeerTube 5.1

We plan to release version 5.1 at the end of February. Here are the new features we are planning to release:

  * Easier management of account registrations (with administrator approval)
  * Developers will be able to take advantage of an **improved API for external authentication plugins** (setting a quota, updating the user, etc.)
  * Optimize **retrieval of video comments**
  * Adding a **Resume Live** button in the video player
  * Improvements and bug fixes (including bugs found during [the end of December load test]( https://www.octopuce.fr/test-de-charge-dun-peertube-en-live-avec-auposte/), in French)

![screenshot](/img/news/roadmap-v6/en/screenshot-bouton-live.jpg)


#### May 2023: PeerTube 5.2

This May we plan to release PeerTube 5.2, which will feature **remote transcoding** ([feature that received many votes!](https://ideas.joinpeertube.org/posts/2/support-for-transcoding-by-remote-workers)). This will reduce the power required for a PeerTube server by delegating power consuming tasks to external machines.
Initially this will only be for hosted videos, but will be designed to be able to evolve (for livestreams for example). Quite a technical challenge ahead!


#### November-December 2023: PeerTube v6

We plan to release the next major version of PeerTube at the end of the year, and the new features are all **inspired by your suggestions** on [our feedback tool *Let's Improve PeerTube*](https://ideas.joinpeertube.org/). You will find:

 * [Adding chapters to videos](https://ideas.joinpeertube.org/posts/6/add-chapters-to-my-videos)
 * [Displaying a preview thumbnail in the progress bar](https://ideas.joinpeertube.org/posts/9/get-a-preview-thumbnail-in-video-progress-bar)
 * [Protecting videos with a password/token](https://ideas.joinpeertube.org/posts/7/protect-video-viewing-with-a-password-token)
 * [The possibility to upload a new version of your video](https://ideas.joinpeertube.org/posts/3/upload-a-new-version-of-my-video)


#### But also...

At the beginning of this year, we welcome Wicklow in the team, for a 6 months internship. This is an opportunity to support the PeerTube developer and to familiarize more people with the code base of the software.

We will also of course continue to **fix bugs, clean up the code and improve the architecture** of the software and **support external and community development**, such as [the Live-Chat plugin](https://www.john-livingston.fr/foss/article/peertube-chat-plugin-quick-feedbacks-about-a-live-stress-test).

Finally, we will work on **content curation** for our [Peer.tube](https://peer.tube/) showcase platform (yes, with a dot in the middle!), to allow us to present [a gateway to PeerTube](https://framablog.org/2022/12/08/framasoft-2022-a-casserole-cooked-up-thanks-to-you-thanks-to-your-donations/#peerdottube), which looks like Framasoft.

![](/img/news/roadmap-v6/en/Peertube-v5_by-David-Revoy.jpg)

PeerTube, like all our projects, is **mainly funded by donations to our association**. On this roadmap, only the remote transcoding feature of v5.2 is already funded, thanks to a donation from the [NLnet foundation](https://nlnet.nl/).

Do you want to help us achieve this roadmap? You can support us **by contributing to PeerTube**, **by sharing this information** and (if you can afford it) [by making a donation to Framasoft](https://support.joinpeertube.org/en/).

Thank you in advance for your support!
Framasoft
