---
id: peertube-app
title: "PeerTube mobile app: discover videos while caring for your attention"
date: December 9, 2024
---

Today, at Framasoft (bonjour!), we publish the very first version of the PeerTube Mobile app for android and iOS. A lot of care went into its conception, to help a wider audience watch videos and discover platforms, while not getting their attention (and data) exploited.


#### Another step into PeerTube growth

Even though we have been developing and maintaining the PeerTube software for 7 years, we, [at Framasoft, are far from being an IT company](https://framasoft.org/). First because **we are a not-for-profit** (funded through donations, you can support us [here](https://support.joinpeertube.org/)), and then because **our goal is, actually, to help others educate themselves on digital issues, surveillance capitalism**, etc. and to give them tools that helps them get digitally emancipated.

**Developing PeerTube has been, to us, an (happy) accident**. We wanted to show that with one paid developer (for the first six years, then two), very little means (~ €650,000 over 7 years) and lots of community contributions, we can create a radical alternative to YouTube and Twitch. It also took a lot of patience. From the get go, **we knew we needed to aim for a slow but steady pace of growth** for the software, the network of video platforms it federates, the whole ecosystem and the audiences it reached.

<div class="news-iframe">
   <div style="position: relative; padding-top: 56.25%">
      <iframe title="Peertube presentation at NGI forum 2023 - by Pouhiou" width="100%" height="100%" src="https://framatube.org/videos/embed/5ddc8a25-33be-4a93-b710-bef1b6145d4e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;"></iframe>
   </div>
</div>

Videos and live-streams are increasingly watched on mobile devices. We knew **the next step to widen the audience of the PeerTube network of platforms was to develop a mobile client**. Last year, we decided to hire [Wicklow (who completed his last internship, before graduating, here with us)](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/), to train him on mobile technologies, develop a mobile app, while continuing to get familiar with PeerTube's core code.

#### Getting funded and getting help

This was (and still is) a big decision: a new hire needs to be funded (our huge thanks to [NLnet](https://nlnet.nl/) and the [NGI0 Entrust program](https://nlnet.nl/entrust/)!), and we want to stay a small structure, so we don't have lots of room in our team. In hindsight, though, we believe it was the right one.

We surrounded ourselves with [Zenika](https://www.zenika.com/), to get help on architecture and experience on mobile strategy. We soon realized that peer-to-peer video sharing wouldn't be a wise strategy on mobile devices. After benchmarking different technologies, Wicklow picked Flutter for the development.

[La Coopérative des Internets (French design workers-owed-company)](https://www.lacooperativedesinternets.fr/), helped us pinpoint the relevant user experience and design an app fit for videos on the fediverse. **We decided, for the first release, to limit the scope of the app to the "spectator use-case"**: browsing and watching videos.

We plan to share all their reports soon (early 2025), as soon as we put in the final touches. We hope that sharing this expertise and experience will help other FLOSS initiatives in their endeavor.

In the meanwhile, the PeerTube Mobile app is (as always with us) Free-libre and open-source, and you can [find the source code here on our repository](https://framagit.org/framasoft/peertube/mobile-application).

<div class="news-img-columns">
  <img src="/img/news/peertube-app/en/Peertube-app-welcome.jpg" alt="image welcome page" />
  <img src="/img/news/peertube-app/en/Peertube-app-video-player.jpg" alt="image player" />
</div>

#### Fediverse complexities made simple

This preparatory work helped us realize that a mobile client was **an amazing opportunity to simplify the PeerTube experience**. PeerTube is not a video platform: it's a network of video platforms, each with their own rules, means and focus, that can choose to federate with others (or not).

It is, by design, more complex than a centralized platform. One of the main feedback we got from video enthusiasts was

> "I don't know where to get an account. I don't know where to search & find videos" (even though we maintain [SepiaSearch](https://sepiasearch.org/)).

![SepiaSearch homepage img](/img/news/peertube-app/en/2024-12-sepia-search-screenshot-EN.jpg)

##### Local account

Within a mobile client, we can create some kind of local account, directly on your device, so you get your watch-list, playlists, faves, etc. **It saves you the hassle of finding a platform where you'd need to create an account** if you just want to enjoy video content.

![image watch later](/img/news/peertube-app/en/Peertube-app-watch-later.jpg)

##### Explore platforms

We can also include a search engine and an interface to explore the federation of PeerTube platforms and find videos suited to your interest. Not everyone knows [SepiaSearch](https://sepiasearch.org/) (and other fediverse search engines) exists: **you get it from the get go, in your pocket**.

![image explore](/img/news/peertube-app/en/Peertube-app-explore.jpg)

##### Highlighting platforms' diversity

Finally, we can present content in a way that highlights the platforms, and show you where the videos/channels you watch are hosted. Differentiating platforms is **a practical, visual way of introducing the concept of federation** to a wider audience.

![image explore platforms](/img/news/peertube-app/en/Peertube-app-explore-2.jpg)



#### Designing out dark patterns

Humility check: a small French nonprofit will never have Google's workforce nor Amazon's money (and vice versa). But **we have an edge: we are not constrained by surveillance capitalism rules**, and its captology models.

> Neither PeerTube nor the mobile app have any interest into grabbing your attention, forcefeeding you ads and milking behavioural and personal data from you.

That is how **we freed the design from toxic design patterns such as doom scrolling, curated feeds, needy notifications and so on**.

It might sound obvious, but it takes real effort to concieve an interface cleaned from what has unfortunately became the new normal. Even more if you need to keep it familiar enough so it says easy to use.

![image see more](/img/news/peertube-app/en/Peertube-app-show-more.jpg)

#### A very first build, limited by (play & i) stores

We knew beforehand that **fitting into Google's PlayStore and Apple AppStore would be a challenge**. They clearly weren't ready to host a client for (not-a-platform but) a network of autonomous video-sharing platforms, published by a small French nonprofit, funded through its independent donation website.

We knew about the [issues encountered by Thorium](https://github.com/sschueller/peertube-android/issues/302) (another PeerTube mobile client). We got help and advices from Gabe, who develops [the streaming tool Owncast](https://owncast.online/) (may your keyboard always repel crumbs and click smoothly), and [encountered many obstacles](https://laurenshof.online/owncast-and-the-app-store/)... We knew about all that but, oh my Tux, it was a wild ride.

After jumping though hoops, here we are, you can download the PeerTube mobile app here:

<div style="display: flex; flex-wrap: wrap;  align-items: baseline; justify-content: center; gap: 1rem">
  <a href="https://f-droid.org/packages/org.framasoft.peertube/" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Get_it_on_F-Droid.png" alt="logo F-Droid"/></a>
  <a href="https://play.google.com/store/apps/details?id=org.framasoft.peertube" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Google-play111.png" alt="logo google store"/></a>
  <a href="https://apps.apple.com/fr/app/peertube/id6737834858" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/fr/Apple-store111.png" alt="logo App store" /></a>
</div>

<p class="text-center mt-3">
  <a target="_blank" rel="noopener noreferrer" href="https://asso.framasoft.org/dolo/h/peertube-apk-latest" class="jpt-secondary-button jpt-link-button ">Download the lastest apk (Android/Advanced)</a>
</p>

#### (un-)Limiting the federation

To get through Apple's (and, in a lesser way, Google's) validation processes, we had to present the mobile app with a curated "allowlist" of PeerTube platforms that meet their standards.

Here is the state of those limitations right now:

* **Apple AppStore**: limited to a very strict allowlist. Truth be told, a week before release, we are still unsure of being validated. Once we manage it, we'll see how to widen the list & let users add platforms they want
* **Google Play Store**: limited allowlist, but users can already add the platforms they want. We plan to widen the allowlist next
* **F-Droid** and direct download apk: all PeerTube platforms we have indexed on [SepiaSearch](https://sepiasearch.org/) are available. If an instance isn't declared to our index or is moderated, you can add it manually.

![image PeerTube plaforms img](/img/news/peertube-app/en/Peertube-app-plaforms.jpg)

We cannot stress enough how **their stores are not ready for independent solidarity-oriented networks**. For exemple, a small "support us" donation link in our website footer or even on one of the allowed platforms triggered a "nope" from Apple.

And that's consistent: as seen in [their fight with Epic](https://en.wikipedia.org/wiki/Epic_Games_v._Apple) (owners of Fortnite) Apple take their share in every in-app purchases. They have an economic interest to keep your expenses enclosed in their ecosystem. Please, please: consider getting your freedom back ;).

![img desappointed](/img/news/peertube-app/en/expected-nothing.jpg)

#### Coming soon, in the PeerTube App

Fitting into Apple's (and Google's) very small boxes took time and energy, more than what we expected. We decided to release a first (incomplete) version of the app in December anyway, and gradually improve on it.

Here are the **features we plan to develop and share for the PeerTube app**:

* Soon (early 2025)
	* Finalize and publish design and mobile strategy reports
	* Publish documentation
	* Play video in background
	* Log in to one's account, gets subscriptions, comment videos
	* next video recommandation
	* improve on the limited platforms list situation
* Then (mid 2025 (if funded))
	* adapt to tablets
	* adapt to TVs (AndroidTV... AppleTV will depend on their limitations)
	* Watch offline (for downloadable content)


Right now, we are still waiting to secure funding for those mid-2024 features (for which we have requested a NLnet grant).

Depending on the app success and usage, **we would love to add the content creator usecase to the app**. But that's a big one: upload and publish a video, manage one's content, create a livestream, etc. We are still wondering **where, when and how to get funds for this undertaking**.

![David Revoy Illustration](/img/news/peertube-app/en/PeerTube-app-CC-BY-David-Revoy.jpeg)

#### Care, Share and Contribute!

**This is the part where we need you**.

We hope you will **enjoy this app, download and use it, and share it** with your friends. This is a new gateway to promote PeerTube content, get audience to fabulous content creators, entice them to share more and boost that virtious loop.

This app is also **a way of showcasing how media could be presented**, when they are made with care for your agency and attention. More than ever: **sharing is caring**.

You can also **contribute by reporting bugs** (within the app), helping on the code ([here is the git repository](https://framagit.org/framasoft/peertube/mobile-application)), and translating the interface. This is an important one: right now, the App is only available in English and French. **[Your language contributions are welcomed](https://weblate.framasoft.org/projects/peertube-app/peertube-app/) here on our translation platform**.

Obviously, we plan to maintain the app, add translations, implement bugfixes and security updates when needed: but this has a cost. **We need to secure Framasoft's 2025 budget** to make Wicklow's position permanent in our team (which is a priority to us). **Our donation campaign is active right now**, you can add your support [here](https://support.joinpeertube.org/) (and thanks!).

![David Revoy Illustration 20 years](/img/news/peertube-app/en/20-ans-CC-BY-David-Revoy.jpeg)

---

You can help us continue to improve PeerTube by sharing this information, [suggesting improvements](https://ideas.joinpeertube.org/) and, if you can afford it, making [a donation to Framasoft](https://support.joinpeertube.org/), the association that develops PeerTube.

Thanks in advance for your support!
Framasoft

<div style="display: flex; flex-wrap: wrap;  align-items: baseline; justify-content: center; gap: 1rem">
  <a href="https://f-droid.org/packages/org.framasoft.peertube/" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Get_it_on_F-Droid.png" alt="logo F-Droid"/></a>
  <a href="https://play.google.com/store/apps/details?id=org.framasoft.peertube" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Google-play111.png" alt="logo google store"/></a>
  <a href="https://apps.apple.com/fr/app/peertube/id6737834858" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/fr/Apple-store111.png" alt="logo App store" /></a>
</div>

<p class="text-center mt-3">
  <a target="_blank" rel="noopener noreferrer" href="https://asso.framasoft.org/dolo/h/peertube-apk-latest" class="jpt-secondary-button jpt-link-button ">Download the lastest apk (Android/Advanced)</a>
</p>
