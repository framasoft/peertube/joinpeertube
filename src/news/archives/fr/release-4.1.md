---
id: release-4.1
title: La 4.1 de PeerTube est sortie !
date: February 23, 2022
---

Bonjour à toutes et à tous,

Nous vous invitons à découvrir les améliorations et fonctionnalités de cette dernière version de PeerTube. Petit tour des nouveautés !


#### Un lecteur vidéo encore plus pratique, surtout sur les mobiles

Afin de rendre plus agréable la lecture des vidéos sur PeerTube, et particulièrement à partir de supports mobiles, nous avons amélioré plusieurs aspects du lecteur vidéo. Désormais, lorsque vous appuyez au centre d'une vidéo, un bouton s'affiche en transparence et permet de lancer / mettre en pause facilement la lecture sans passer par la barre de contrôle.

Nous avons agrandi la taille de cette barre de contrôle (située en bas du lecteur) : vous pouvez désormais accéder plus facilement au bouton de lecture/pause et aux réglages du son, de l'affichage ou de la vitesse de lecture.

Pour celles et ceux qui regardent des vidéos depuis un support mobile, nous avons paramétré un affichage automatique en mode paysage des vidéos lorsque la lecture en mode plein écran est activée. Et si vous appuyez deux fois sur la partie droite ou gauche du lecteur vidéo, cela vous permet d'avancer / reculer la lecture de 10 secondes sans utiliser la barre de contrôle. Vraiment pratique pour naviguer facilement dans une vidéo !

Aviez-vous remarqué qu'en appuyant sur la touche `?`, vous faisiez apparaître la liste des raccourcis clavier disponibles ? Cette fonctionnalité existe depuis un moment, mais nous ne l'avions pas encore mise en avant. Comme nous avons apporté quelques corrections au système, c'est l'occasion de vous la faire découvrir.

![](/img/news/release-4.1/fr/FR-Raccourcis-clavier.jpg)


#### Un système de plugins amélioré

Les améliorations que nous avons apportées au système de plugins permettent aux développeurs qui le souhaitent de créer de nouveaux types de plugins :

  * **des plugins pour créer des pages spécifiques intégrées à l'interface de PeerTube**
  Certaines instances souhaitant ajouter des contenus autres que les pages par défaut pourront désormais le faire sans perdre le contexte graphique de l'interface PeerTube.

  * **des plugins pour ajouter un champ au formulaire de publication d'une vidéo**
  Il était déjà possible d'ajouter à une vidéo de nouveaux champs de description via un plugin, mais ceux-ci étaient visibles dans un onglet spécifique ("Paramètres du plugin"). Il est désormais possible de faire apparaître ces nouveaux champs dans l'onglet "Informations basiques", ce qui les rend plus accessibles.

Pour rappel, les administrateur⋅ices d'instances peuvent activer / désactiver les plugins dans le menu `Administration`, onglet `Plugins / Thèmes` et vous pouvez découvrir notre [sélection de plugins](https://joinpeertube.org/plugins-selection).

#### De nouveaux filtres sur les résultats de recherche

Désormais, lorsque vous faites une recherche depuis la barre de recherche de votre instance préférée, il est possible de filtrer les résultats pour ne faire apparaître qu'un seul type de résultat au choix parmi `vidéos`, `chaînes` ou `listes de lecture`. Bien pratique pour trouver les chaînes ou des listes de lecture sur un sujet spécifique.

![](/img/news/release-4.1/fr/FR-Filtre-types-resultats.jpg)


#### De nouvelles possibilités de personnalisation des instances

Afin que les personnes administrant une instance PeerTube puissent davantage paramétrer les usages au sein de leur instance, nous leur permettons désormais de :

  * **définir le type de visibilité par défaut sur les vidéos ajoutées**
  Jusqu'à ce jour, cette visibilité par défaut était "publique" pour toute vidéo ajoutée. Les admins peuvent désormais décider que toutes les vidéos publiées sur leur instance le seront par défaut en visibilité non listée / privée / interne (au choix). Il reviendra donc aux utilisateur⋅ices de modifier ce paramètre dans un second temps.

  * **définir le type de licence par défaut sur les vidéos ajoutées**
  Le champ décrivant la licence d'une vidéo publiée était, jusqu'à maintenant, non-complété par défaut. Il est désormais possible de choisir une valeur par défaut pour ce champ et l'appliquer à l'ensemble des contenus qui seront publiés sur une instance. Les vidéastes auront alors la possibilité de modifier ce choix par défaut.

 * **désactiver certaines fonctionnalités**
 Parce que vous avez été nombreux·ses à nous le demander, il est désormais possible de désactiver par défaut la possibilité de commenter et/ou de télécharger une vidéo.

 Toutes ces modifications sont à réaliser dans le fichier de configuration de l'instance.

#### Du nouveau pour la diffusion en pair-à-pair

PeerTube utilise un protocole pair-à-pair (P2P) pour diffuser les vidéos très regardées par les internautes (vidéos virales), ce qui permet d'alléger la charge des instances qui les hébergent. Dorénavant, les administrateur⋅ices peuvent désactiver par défaut cette fonctionnalité. Les utilisateur⋅ices (connecté⋅es ou anonymes) peuvent toutefois le réactiver s'iels le souhaitent.

Il est aussi possible de désactiver le pair-à-pair lorsque qu'on souhaite intégrer une vidéo à une page web externe. Pour cela, il suffit de décocher la case `P2P` qui apparaît dans l'onglet `Intégration` de la fenêtre de partage d'une vidéo.

![](/img/news/release-4.1/fr/FR-embed-p2p.jpg)

#### Et aussi

Nous avons ajouté la possibilité de contrôler le comportement de la connexion des utilisateurs : si un ou plusieurs plugins d'authentification externes sont installés sur une instance, des boutons spécifiques pour se connecter à chaque type d'authentification externe apparaîtront en dessous du formulaire de connexion. Si un seul plugin d'authentification externe a été installé, les administrateur⋅ices de l'instance peuvent activer une redirection automatique des utilisateur⋅ices sur la plateforme d'authentification externe lorsque ces derniers cliquent sur `Se connecter`.

Beaucoup d'autres améliorations ont été apportées dans cette nouvelle version. Vous  pouvez voir la liste complète (en anglais) sur [https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md).

Merci à tous les contributeur⋅ices de PeerTube !
Framasoft

