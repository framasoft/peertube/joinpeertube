---
id: roadmap-v3
title: Feuille de route de la v3 de PeerTube
date: May 27, 2020
---

<p>Hier, nous avons publié notre <a href="https://joinpeertube.org/roadmap" target="_blank">nouvelle feuille de route pour la v3 de PeerTube</a> qui détaille les fonctionnalités clés telles que :</p><ul><li>recherche globale</li><li>outils de modération</li><li>plugin &amp; playlists</li><li>streaming live en pair-à-pair</li></ul><p>Veuillez lire <a href="https://framablog.org/2020/05/26/nos-plans-pour-peertube-v3-collecte-perlee-du-live-pour-cet-automne/">notre blog</a> pour en savoir plus sur nos choix concernant les six prochains mois de développement.</p><p>Cette version 3 de PeerTube devrait voir le jour en novembre 2020.</p><p>D'ici là, nous espérons que vous <a href="https://joinpeertube.org/roadmap" target="_blank">partagerez et soutiendrez la roadmap sur JoinPeertube</a>.</p><p><span>Librement,</span><br> Framasoft </p>
