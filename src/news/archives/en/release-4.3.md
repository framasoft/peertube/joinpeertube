---
id: release-4.3
title: PeerTube v4.3 is out!
date: September 21, 2022
---

Hi everybody,

We are pleased to announce this new version of PeerTube!

Ability to automatically import videos from a remote channel, UI improvements, more instance customization and much more...  Let's look around and see what it brings us!
PeerTube v4.3 is out!

#### Automatic import of videos from a remote channel

This is **THE** feature you've been waiting for: ability to automatically import all videos from a remote channel (from another video platform) into one of your PeerTube channels. Really useful if you are a video maker publishing on several platforms who wants to make your channels visible on PeerTube. You can also use this feature to group videos from several remote channels in the same channel. A special thank to [Florent](https://github.com/fflorent), one of [Skeptikón PeerTube Instance](https://skeptikon.fr/) administrators, for developing this new feature.

To add a remote channel synchronization, simply click on the button *Add synchronization* located in *My library* menu / *Channels* tab / *My synchronizations* button and add the requested information:

  * *Remote channel URL*: copy the channel's URL you want to import
  * *Video Channel*: choose from the drop-down menu the channel you wish to synchronize with

![](/img/news/release-4.3/en/EN-new-synchronization.jpg)

Once this synchronization is created, PeerTube will watch the remote channel for new publications and will automatically import new videos into your channel.

You can also use this synchronization tool to import content from a remote playlist.

#### PeerTube UI improvements

In order to make PeerTube User Interface even more user-friendly, we are working with a designer from [la Coopérative des Internets](https://www.lacooperativedesinternets.fr/) and have implemented some of her suggestions in this release.

We have reviewed the account creation page process to make some of the information you need to know before registering more explicit:
  1. we explain why creating an account
  2. we display instance terms and invite you to accept them
  3. you are asked to fill the elements necessary to create your account with some explanations
  4. you are asked to create your first channel (but you can skip this step)
  5. your account is created

![](/img/news/release-4.3/en/EN-subscribe-compare.jpg "on your left the old account registration page and on you right the new one")

We also changed the location of the login page's elements to place the information message at the top.
To make it more visible, the search bar is now at the top center of each page.
Finally, we have increased the size and lightened the color of the default font to make it more accessible. These small changes will make PeerTube more comfortable for you to use.


#### Better integration of videos and live streams

As you probably know, it is possible to embed a PeerTube video player in other websites (by copying a small html code that you get by clicking on the *Share* button / *Embed* tab). Practical, isn't it?

Until now, we had a small display issue when embedding live videos: before and after the live schedule, the embedded player did not display anything and gave the impression that it was not working. To remedy this, we have added informative texts displayed before the start of live event (*This live has not started yet*) and once it has ended (*This live has ended*).

We have also added automatic playback to the embedded videos when the live broadcast starts. It is no longer necessary to refresh the web page regularly. Please note that some browsers are set by default to block automatic video playback and you will have to change your settings to benefit from this new feature.


#### More instances customizations

We improved PeerTube admin management. As an administrator, you can now:

 * **execute batch actions on federated instances**
Performing batch actions (selecting several items in order to apply to all of them the same treatment) was already available for the *Overview* tab pages (users, videos and comments). It is now available for the *Federation* tab pages. Very useful to delete several subscriptions at the same time or to reject some followers.

 * **disable transcoding of uploaded video or live stream original resolution**
As an instance's administrator, you can set the resolutions in which uploaded videos are transcoded. Until now, PeerTube always transcoded the original video file, even if its resolution was not enabled. This can be cumbersome for some instances. It is now possible to disable the original video transcoding when its resolution is higher than the maximum resolution set. To do this, go to the *Administration* menu, *Configuration* tab, then *VOD Transcoding* and check the box in front of the text *Always transcode original resolution*.

 * **delete a specific video file**
Storing multiple versions of a video can quickly increase your instance used disk space. You can now delete specific files from the web interface. This can be very useful if, for example, you want to delete all video files that are bigger than the resolution you have enabled for your instance.

![](/img/news/release-4.3/en/EN-suppression-video-specifique.jpg)

#### And also

PeerTube supports 2 new languages: [Toki Pona](https://en.wikipedia.org/wiki/Toki_pona) and Croatian!

We have also made several optimizations to make the software more scalable. As a result, its performance has been greatly improved.

There are many other improvements in this version. You can read the whole list on https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md.

Finally, we would like to remind you that we launched in July a feedback tool to collect your needs on PeerTube. We want to know what content creators, video-lovers and non-tech-savvy people miss from PeerTube or what changes/new experiences they would like to have. We invite you to have your say at [Let's improve PeerTube!](https://ideas.joinpeertube.org/). And if you are not necessarily inspired, you can always vote for one of the 80 ideas already posted. We'd like to take this opportunity to thank all the people who made these proposals and all those who voted.

Thanks to all PeerTube contributors!
Framasoft
