---
id: release-7.0
title: "PeerTube v7 : offrez un nouveau look à votre plateforme vidéo !"
date: december 17, 2024
---

Dotée d\'un tout nouveau design, la nouvelle interface de PeerTube n\'est pas uniquement plus esthétique. Elle est aussi plus simple, plus facile à utiliser et à comprendre, et plus accessible. C\'est une nouvelle ère pour ce logiciel qui permet aux vidéastes d\'avoir leur propres plateforme de vidéo, de les modérer et de les connecter entre elles.

#### Avancées rapides de l\'écosystème PeerTube

Il y a sept ans, PeerTube était principalement un outil connu uniquement des passionnés de logiciels libres à la pointe de la technologie. Il est ensuite devenu plus populaire parmi les créatrices et créateurs de contenu qui voulaient auto héberger un miroir de leurs chaînes YouTube/Twitch ; et parmi les communautés qui voulaient créer et modérer un espace inclusif (personnes sourdes, personnes queers, etc.).

Aujourd\'hui, **PeerTube connaît un succès croissant** auprès de vidéastes qui publient du contenu original (ou du contenu réservé à leur communauté), de médias alternatifs et de diverses institutions : universités, ministères de l\'éducation, archives de la télévision et des radios nationales, etc.

> Les structures publiques ont souvent besoin de partager des contenus vidéo sans avoir recours à des mécanismes qui attirent l\'attention ou qui exploitent les données.

[![illustration with the PeerTube mascot and the motto \"building a free internet of the future\"](/img/news/release-7.0/fr/peertube-future.jpg "Découvrez l\'histoire et les valeurs de PeerTube grâce à cette interview de l\'Association pour la communication progressive (en anglais)")](https://www.apc.org/node/40437)

Pour nous, il s\'agit d\'une nouvelle avancée dans l\'évolution de l\'audience de PeerTube.

Cette année, nous avons donc demandé à [La Coopérative des Internets](https://www.lacooperativedesinternets.fr/) de mener une étude UX approfondie (avec entretiens, tests, etc.) et de nous aider à entamer une refonte complète du design. Notre objectif était d\'améliorer PeerTube pour qu\'il réponde au mieux aux besoins de ces nouveaux publics. Nous leur avons clairement donné carte blanche pour tout remettre en question : les couleurs, le vocabulaire, la mise en page\...

**Nous sommes fiers de vous présenter [cette v7 de PeerTube](https://github.com/Chocobozzz/PeerTube)**, qui pose les bases d\'une refonte complète du logiciel et de son design.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://github.com/Chocobozzz/PeerTube">
      Voir le code source
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>

#### Petit aperçu : des thèmes, des nouvelles couleurs et du vocabulaire

Le design de PeerTube, ses couleurs, son vocabulaire\... Tout s\'est construit au cours des sept dernières années, au fur et à mesure de notre apprentissage sur le tas et de l\'aide apportée par la communauté. Ce nouveau design a été l\'occasion **de prendre un peu de recul et de donner un sens, des intentions à l\'interface**.

![Image de l\'interface light beige de PeerTube](/img/news/release-7.0/fr/FR_beige_light_interface-1024x640.png)

Le nouveau thème par défaut, beige clair, est plus chaleureux et plus agréable à l\'œil que le thème original, noir et orange. Un thème brun est aussi disponible, pour qui préfère les affichages sombres. Ces deux thèmes ont pour but de **faciliter la navigation sur les vidéos**.

La création de ces nouveaux thèmes a été l\'occasion de **nettoyer et de simplifier le code de l\'interface** (en particulier : nettoyer les CSS, en se concentrant sur les variables), tout en limitant les ruptures avec les thèmes personnalisés préexistants. Il est maintenant **beaucoup plus facile de créer de nouveaux thèmes pour PeerTube**, nous espérons que vous nous partagerez vos réalisations !

![Image de l\'interface dark marron de PeerTube](/img/news/release-7.0/fr/FR_dark_marron_interface-1024x640.png)

Le glossaire de PeerTube a également été mis à jour. Ce n\'est pas pour rien que **nous utilisons désormais le terme « plateforme(s) »** pour parler de tous les serveurs sur lesquels PeerTube a été installé. Les appeler « instance(s) » est la norme dans le monde technique des enthousiastes d\'ActivityPub. Mais pour celles et ceux qui n\'ont pas le privilège de connaître le Fediverse et son protocole, le mot plateforme est plus explicite, et facilite l\'inclusion.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://joinpeertube.org">
      Site PeerTube
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>

#### Une mise en page plus simple et plus pertinente

Il y a beaucoup d\'informations à afficher sur une page PeerTube. Les présentations et les menus se sont développés au fil du temps durant sept années de développement\... et avaient bien besoin d\'être révisés ! **Ces nouveaux menus et pages ont été complètement retravaillées** pour mettre en avant les informations importantes et permettre de trouver plus facilement le contenu que l\'on recherche.

![Image de l\'interface de PeerTube en utilisateur anonyme](/img/news/release-7.0/fr/FR_anonyme_interface-1024x640.png)

Par exemple, les créateurs et créatrices de contenu accédaient à leurs chaînes et aux vidéos téléchargées dans leur bibliothèque (où tout utilisateur de PeerTube peut accéder aux listes de lecture, à l\'historique, etc. des vidéos visionnées). **Désormais, dans PeerTube v7, il existe une nouvelle section appelée « Espace vidéo »** spécifique aux besoins des créateurs de contenus.

De la même manière, les pages « Admin » **pour qui administre leur propre plateforme PeerTube** ont été séparées en une page « Aperçu » (pour obtenir plus d\'informations sur les utilisateurs, les vidéos hébergées, etc.), **une page « Modération »** (pour gérer les abus, les blocages et les inscriptions), **et une page « Paramètres »** (pour accéder à la configuration, les runners, etc.).

![Image de la partie admin de PeerTube](/img/news/release-7.0/fr/FR_admin_interface-1024x640.png)

Les différentes pages qui affichaient les vidéos d\'une plateforme PeerTube (Récemment ajoutées, Vidéos locales, Tendances) ont été fusionnées en une page « Parcourir les vidéos », qui inclut des options de filtres rapides pour afficher les mêmes sélections de contenu d\'une manière plus directe.

La nouvelle disposition de **la page « Découvrir des vidéos »** a été conçue dans le même esprit, nous espérons qu\'elle satisfera même les utilisateurs les plus curieux.

Enfin, la barre de gauche et les menus de l\'en-tête ont été réorganisés afin d\'illustrer ces changements et rendre la navigation encore plus intuitive. Vous pouvez désormais **accéder aux paramètres de votre compte et aux notifications à partir du menu d\'en-tête**, coutume courante sur d\'autres sites web.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://sepiasearch.org/">
      Moteur de recherche PeerTube
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>

#### Afficher simplement la diversité des vidéos

L\'un des principaux retours de personnes découvrant PeerTube était que l\'ancienne interface était confuse, qu\'il était difficile pour une utilisatrice de savoir où il se trouvait et d\'où provenaient les vidéos.

C\'est pourquoi, dans PeerTube v7, nous avons ajouté **plus de moyens pour les propriétaires de plateformes de personnaliser et d\'identifier leurs plateformes** : ajouter facilement une bannière (utilisée sur les pages, l\'exploration de l\'application mobile, et notre moteur de recherche [SepiaSearch](https://sepiasearch.org/)) et une icône de plateforme (utilisée par l\'application mobile). De plus, le nom et la description de leur plateforme sont maintenant affichés pour les utilisateurs non-enregistrés dans le menu de gauche.

![Capture d\'écran des résultats de recherche de la plateforme sur JoinPeerTube](/img/news/release-7.0/fr/platform-list-sepiasearch-1024x640.jpg)

**La manière dont les miniatures vidéo apparaissent a également été modifiée**, dans toutes les pages qui listent des vidéos. Les avatars des chaînes sont mis en avant afin de faciliter l\'identification des créateurs, les titres sont mis en évidence, la date et le nombre de vues de la vidéo sont présents mais atténués. Ces changements rendent les pages qui répertorient les vidéos plus lisibles et simplifient l\'identification de la vidéo que vous souhaitez regarder.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://joinpeertube.org/news/peertube-app">
      Application mobile PeerTube
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>

#### L\'accessibilité mise à l\'honneur

La refonte nous a également permis de donner **la priorité à l\'accessibilité de l\'interface** (pour les personnes handicapées). [En 2023, nous avions préparé le code et avancé à l\'aide de nos connaissances](https://framablog.org/2023/11/28/peertube-v6-est-publie-et-concu-grace-a-vos-idees/)\... pour que l\'audit complet d\'accessibilité prévu en 2024 (grâce au consortium NGI Entrust) apporte autant d\'améliorations nouvelles et détaillées que possible.

Grâce à l\'audit, **de nombreux points ont pu être améliorés** : les contrastes de couleurs et les thèmes, la barre de progression, plusieurs composants et divers problèmes liés aux lecteurs d\'écran ont été corrigés. Les labels manquants sur les éléments interactifs ainsi que des liens « passer le menu » ont été ajoutés, les liens sont soulignés. La navigation au clavier a été perfectionnée et les composants d\'une dépendance non accessible ont pu être réimplémentés.

![capture d\'écran de la page vidéo vrowse sur peertube v7](/img/news/release-7.0/fr/browse-videos-EN-1024x640.jpg)

Nous pensons sincèrement que **PeerTube a rattrapé son retard en matière d\'accessibilité et devrait maintenant être conforme aux normes**\... mais nous savons aussi qu\'il est toujours possible de s\'améliorer et **d\'apprendre directement des personnes concernées**.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://framapiaf.org/@peertube">
      Suivre PeerTube sur Mastodon
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>

#### Édition de sous-titres, promotion des vidéos et plus encore\...

Grâce au [nouvel outil de transcription introduit l\'année dernière](https://framablog.org/2023/11/28/peertube-v6-est-publie-et-concu-grace-a-vos-idees/), il est désormais très simple d\'obtenir une transcription ou des sous-titres pour votre vidéo. Mais l\'outil d\'édition des transcriptions/sous-titres était\... hum\... disons sommaire. **Une nouvelle fenêtre modale a été introduite**, pour rendre **l\'édition des sous-titres beaucoup plus aisée**.

![Image montrant l\'interface pour modifier les sous-titres dans PeerTube](/img/news/release-7.0/fr/Captions-FR-1024x640.jpg)

Nous avons accueilli et intégré en amont une **contribution de la communauté sur le SEO** (optimisation pour les moteurs de recherche), pour aider à promouvoir le contenu hébergé sur PeerTube dans les moteurs de recherche. Un avatar de la plateforme apparaît désormais dans les balises opengraph, les comptes et canaux vides sont masqués dans le plan du site, tandis que des balises vidéo supplémentaires y sont désormais présentes.

Enfin, **PeerTube a été traduit en slovaque**.

Nous voulons vraiment prendre le temps de **remercier la communauté qui contribue aux traductions de PeerTube**, jamais nous n\'aurions pensé que notre logiciel serait un jour disponible dans plus de 38 langues.

> Un grand merci à vous tous, personnes merveilleuses, qui avez pris le temps et le soin de [contribuer à notre outil de traduction](https://weblate.framasoft.org/) : vous êtes incroyables !

![sepia, mascotte de peertube, entretenant son jardin](/img/news/release-7.0/fr/2020-05-21_Peertube-Moderation_by-David-Revoy-lowres.jpg "Illustration: David Revoy - Licence : CC-By 4.0")

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://joinpeertube.org/news">
      S'inscrire aux nouvelles PeerTube
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>

#### Plus d\'améliorations à venir\...

**D\'autres chantiers sont prévus dans le cadre de cette refonte de l\'interface**. Nous espérons les livrer dans les premiers mois de 2025. Tout d\'abord, la traduction du **rapport de recherche UX de La Coopérative des Internets** est en voie d\'être terminée. Nous le **publierons** dans l\'espoir qu\'il aidera l\'ensemble de la communauté du Fediverse.

Une fois que les administratrices de PeerTube auront le temps de faire la mise à jour de leurs plateformes, nous **mettrons à jour la documentation de PeerTube** avec de nouvelles captures d\'écran, et les chemins des nouveaux menus.

Les prochaines évolutions seront dédiées à la **simplification de la gestion des chaînes et des vidéos pour les créateurs de contenu** (où plusieurs outils et menus ont été ajoutés au fil des ans). **La catégorisation des vidéos NSFW devrait également être affinée.**

[![Illustration - Dans la mer Sepia, læ poulpe mascotte de PeerTube, dessine un grand chiffre sept avec son encre.](/img/news/release-7.0/fr/PeerTube-v7-CC-BY-David-Revoy-1024x576.jpeg "Illustration: David Revoy - Licence : CC-By 4.0")](https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR)

Évidemment, beaucoup d\'autres projets sont prévus pour **notre feuille de route 2025, mais nous attendons de voir si nous allons obtenir des fonds pour les réaliser** : nous vous tiendrons informées dès que nous en saurons plus !

En ce qui concerne les fonds, nous tenons **à remercier le programme [Entrust du NGI0](https://nlnet.nl/entrust/)** pour sa subvention qui a permis de financer la majeure partie du travail sur cette nouvelle version (et sur l\'application mobile PeerTube que nous avons lancée la semaine dernière). **L\'équipe [NLnet](https://nlnet.nl/) a été un excellent partenaire** dans la gestion de cette subvention, et **l\'audit d\'accessibilité du consortium** a vraiment fait une énorme différence.

<div class="mt-4 mb-4 text-center">
   <a class="jpt-secondary-button jpt-link-button" target="_blank" href="https://ideas.joinpeertube.org/">
      Partagez vos idées et retours sur PeerTube
   </a>
</div>
<div class="mt-4 mb-4 text-center">
   <a class="jpt-primary-button jpt-link-button" target="_blank" href="https://support.framasoft.org/?mtm_campaign=Automne2024&mtm_source=NewsJPTv7FR">
      Soutenir Framasoft
   </a>
</div>
