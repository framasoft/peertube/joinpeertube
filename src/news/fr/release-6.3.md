---
id: release-6.3
title: "Sortie de la v6.3 de PeerTube !"
date: September 18, 2024
---

Dernière version mineure avant la v7, cette mouture n'en est pas moins pourvue de nouveautés intéressantes ! Faisons un tour :)

#### Séparer les flux audio et vidéo pour plus de souplesse

La séparation du flux audio et du flux vidéo permet non seulement de réduire le poids des fichiers sur le serveur puisqu'il n'est plus nécessaire de dupliquer le fichier audio pour chaque qualité de la vidéo, mais il permet aussi de diffuser uniquement l'audio d'une vidéo via une option "Audio seulement" dans le player !

Cette fonctionnalité est très pratique pour diffuser des morceaux de musique qui pourront afficher leur pochette d'album, ou bien pour utiliser moins de bande passante lorsqu'on écoute une conférence dont la vidéo n'est pas nécessaire.

![image animée de l'audio only](/img/news/release-6.3/fr/audio_only_FR.gif)

Cette nouvelle fonctionnalité permet aussi à PeerTube d'être capable d'ingérer un flux Live qui contient seulement de l'audio (donc sans flux vidéo). Ça veut dire qu'il est possible de diffuser de la musique en direct via PeerTube, qui affichera un seulement lecteur *Audio*.

Enfin, nous en avons profité pour simplifier quelques éléments de la modale qui apparaît quand on souhaite télécharger une vidéo : elle affiche désormais les informations essentielles de chaque qualité disponible, avec la possibilité, ou non, d'inclure l'audio dans le fichier à télécharger. Les options avancées restent toujours disponibles en selectionnant *Fichier Vidéos* à côté du titre de la modale *Télécharger*.

![image de la modale de téléchargement](/img/news/release-6.3/fr/download_modal_FR.png)

#### Naviguer dans les sous-titres d'une vidéo

En plus de l'affichage classique des sous-titres intégré dans le lecteur vidéo, il est désormais possible d'avoir un espace latéral sur la droite de la vidéo pour visualiser les sous-titres (autrement appelé *Widget de Transcription*). Ce panneau permet de :

 * suivre en temps réel les sous-titres
 * revenir à une section de la vidéo en cliquant sur une phrase
 * faire une recherche pour retrouver une phrase ou un mot, et ainsi revenir à un endroit précis de la vidéo

![widget de transcription](/img/news/release-6.3/fr/peertube_transcription_widget_recherche.png)

#### Paramétrer Youtube-dl, pour faciliter l'import

Youtube-dl est un outil essentiel pour pouvoir importer les vidéos et les informations associées depuis d'autres plateformes vidéos (comme PeerTube, Youtube, Vimeo et [beaucoup d'autres](https://ytdl-org.github.io/youtube-dl/supportedsites.html)).

Cette nouvelle version de PeerTube ajoute la possibilité pour les admins d'instances de paramétrer plusieurs proxies dédiés à youtube-dl, que PeerTube sélectionnera aléatoirement, permettant de contourner certaines restrictions.

Il est aussi possible d'utiliser désormais une autre version de youtube-dl, binaire, qui contient des dépendances additionnelles permettant d'utiliser des fonctionnalités comme l'impersonnification (c'est-à-dire se faire passer pour un vrai navigateur).


#### Et bien plus

Comme pour chaque version, quelques améliorations d'expériences utilisateurices et d'interfaces ont été réalisées :

 * les bonnes résolutions ont de l'avance sur PeerTube : les vidéos au format *1920x816* sont désormais indiquées comme étant en *1080p* plutôt que *816p*
 * meilleure visibilité des marqueurs des chapitres dans la barre de progression du lecteur : ceux-ci sont désormais affichés sous forme de petits points sur la barre de progression
 * reprise plus fluide de la lecture d'un direct : seul le lecteur vidéo est rechargé
 * ajout de la possibilité de copier facilement les journaux d'erreurs du serveur via un bouton dédié
 * possibilité pour les admins d'instances de changer le nombre maximal d'images par secondes des vidéos (limité par défaut à 60)

Vous pouvez retrouver l'ensemble des corrections de bugs, améliorations et autres changements dans [le journal des modifications (en anglais)](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.3.0) !

Enfin, nous sommes toujours en train de [préparer l'application mobile](https://joinpeertube.org/news/peertube-future-2024) et nous aurons de bonnes nouvelles à annoncer en fin d'année !

---

Vous pouvez nous aider à continuer à améliorer PeerTube en partageant cette information, en [proposant des idées d'améliorations](https://ideas.joinpeertube.org/) (en anglais) et, si vous pouvez vous le permettre, en [faisant un don à Framasoft](https://support.joinpeertube.org/), l'association qui développe PeerTube.

Merci d'avance de votre soutien !
Framasoft

