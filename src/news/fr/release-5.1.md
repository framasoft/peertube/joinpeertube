---
id: release-5.1
title: La 5.1 de PeerTube est sortie !
date: March 28, 2023
---

La version 5.1 de PeerTube est sortie ! Au menu : modération des créations de compte, un bouton pour reprendre le direct, la gestion des plugins d'authentification externe améliorée... et d'autres évolutions bien pratiques. C'est parti pour le tour des nouveautés !

#### Modération des demandes de création de compte

Première nouveauté pour cette version mineure : les demandes de création de compte [peuvent maintenant être validées à priori](https://docs.joinpeertube.org/admin/managing-users#registration-approval) par les administrateurs et administratrices.

Lorsque la fonctionnalité est activée et qu'une personne s'inscrit sur une instance, elle doit remplir un champ (de type « pourquoi je souhaite créer un compte sur cette plateforme ? ») et doit ensuite attendre la validation de son inscription pour accéder à son compte.

![](/img/news/release-5.1/fr/registration-reason_FR.png)

De leur côté, les modérateurs et modératrices visualisent les différentes demandes, et peuvent les accepter ou les rejeter.

![](/img/news/release-5.1/fr/registration-list_FR.png)

Un mail est directement envoyé à l'utilisateur⋅ice lorsque la demande est traitée. Le compte est automatiquement créé lorsque la sollicitation est validée.

![](/img/news/release-5.1/fr/registration-accept_FR.png)

Nous espérons ainsi permettre à différentes instances de ré-ouvrir leurs inscriptions sans risquer de potentielles vagues de spams.

#### Nouveau bouton pour « Reprendre le direct »

Un bouton « reprise du direct » arrive dans le lecteur ! Le bouton est rouge lorsque le lecteur est synchronisé avec le live en cours, et devient gris lorsqu'il ne l'est plus. **Un seul clic qui permet ainsi de resynchroniser le live**, c'est quand même bien pratique !

![](/img/news/release-5.1/fr/screenshot-bouton-live.jpg)

#### Gestion des plugins d’authentification externe améliorée

Les développeurs pourront profiter d'une amélioration de l’API [pour les plugins d’authentification externe](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md#pluginsthemesembed-api) : définition d'un quota pour les utilisateur⋅ices, mise à jour des utilisateur⋅ices, ou encore mise en place d'une redirection automatique sur le service externe lorsqu'une session expire. Ces améliorations ont été financées par le Département de l'Instruction Publique (DIP) de l'État de Genève. Merci à eux !

#### Et aussi...

Cette version comporte différentes améliorations d'accessibilité de l'interface, ainsi que des améliorations de performance (la récupération des commentaires est devenue plus efficace et nous avons optimisé le rendu de l'éditeur de la page d'accueil).

Une autre évolution à noter est l'arrivée de deux nouvelles langues : l'islandais et l'ukrainien. Merci aux contributeurs pour ces traductions !

Nous avons aussi corrigé de nombreux bugs rapportés par la communauté. Nous arrivons maintenant à [plus de 4000 tickets traités](https://github.com/Chocobozzz/PeerTube/issues) depuis le début du projet PeerTube, et ça, ça nous semble énorme ! Ce temps passé à améliorer, maintenir et apporter du support au logiciel, **c'est directement vous qui le financez**, [grâce à vos dons](https://support.joinpeertube.org/fr/). Merci !

Nous espérons que cette nouvelle version vous sera utile et nous remercions encore tous les contributeurs et contributrices de PeerTube !

Framasoft
