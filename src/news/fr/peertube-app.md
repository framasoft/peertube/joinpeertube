---
id: peertube-app
title: "L'application PeerTube Mobile : découvrir des vidéos tout en ménageant votre attention"
date: December 9, 2024
---

Aujourd’hui, chez Framasoft, nous publions la toute première version de l’application PeerTube Mobile pour android et iOS. Beaucoup de soin a été apporté à sa conception, afin d’aider un public plus large à regarder des vidéos et à découvrir des plateformes, sans que son attention (et ses données) ne soient exploitées.

#### Une nouvelle étape dans la croissance de PeerTube

Bien que nous développions et maintenions le logiciel PeerTube depuis 7 ans, nous, chez Framasoft, sommes loin d’être une entreprise informatique. D’abord parce que nous sommes **une association à but non lucratif** (financée par des dons, vous pouvez nous soutenir [ici](https://support.joinpeertube.org/)), et ensuite parce que **notre but est, en fait, d’aider les autres à s’éduquer sur les questions numériques, le capitalisme de surveillance**, etc. et de leur donner des outils qui les aident à s’émanciper numériquement.

**Le développement de PeerTube a été, pour nous, un (heureux) accident**. Nous voulions montrer qu’avec un développeur rémunéré (pendant les six premières années, puis deux), très peu de moyens (~ 650 000 € sur 7 ans) et beaucoup de contributions de la communauté, nous pouvons créer une alternative radicale à YouTube et Twitch. Il a également fallu beaucoup de patience. Dès le départ, **nous savions que nous devions viser un rythme de croissance lent mais régulier** pour le logiciel, le réseau de plateformes vidéo qu’il fédère, l’ensemble de l’écosystème et le public qu’il atteint.

<div class="news-iframe">
   <div style="position: relative; padding-top: 56.25%">
      <iframe title="Peertube presentation at NGI forum 2023 - by Pouhiou" width="100%" height="100%" src="https://framatube.org/videos/embed/5ddc8a25-33be-4a93-b710-bef1b6145d4e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;"></iframe>
   </div>
</div>

Les vidéos et les flux en direct sont de plus en plus regardés sur des appareils mobiles. Nous savions **que la prochaine étape pour élargir l’audience du réseau de plateformes PeerTube était de développer un client mobile**. L’année dernière, nous avons décidé d’embaucher [Wicklow (qui a effectué son dernier stage chez nous avant d’obtenir son diplôme)](https://framablog.org/2023/11/28/peertube-v6-est-publie-et-concu-grace-a-vos-idees/), pour le former aux technologies mobiles et développer une application mobile, tout en continuant à se familiariser avec le code de base de PeerTube.

#### Obtenir un financement et de l’aide

C’était (et c’est toujours) une décision importante : une nouvelle embauche doit être financée (un grand merci à [NLnet](https://nlnet.nl/) et au [programme Entrust du NGI0](https://nlnet.nl/entrust/) !), et nous voulons rester une petite structure, donc nous n’avons pas beaucoup de place dans notre équipe. Avec le recul, nous pensons que c’était la bonne décision.

Nous nous sommes entourés de [Zenika](https://www.zenika.com/), pour obtenir de l’aide sur l’architecture et de l’expérience sur la stratégie mobile. Nous nous sommes vite rendu compte que le partage de vidéos en peer-to-peer n’était pas une stratégie judicieuse sur les appareils mobiles. Après avoir comparé différentes technologies, Wicklow a choisi Flutter pour le développement.

[La Coopérative des Internets](https://www.lacooperativedesinternets.fr/) (une scop de designers) nous a aidés à identifier l’expérience utilisateur pertinente et à concevoir une application adaptée aux vidéos sur le Fediverse. **Nous avons décidé, pour la première version, de limiter le champ d’application de l’app au « cas d’utilisation spectateur »** : parcourir et regarder des vidéos.

Nous prévoyons de partager tous les rapport prochainement (début 2025), dès que nous aurons mis les dernières retouches à l’application. Nous espérons que le partage de cette expertise et de cette expérience aidera d’autres initiatives FLOSS dans leurs efforts.

En attendant, l’application PeerTube Mobile est (comme toujours avec nous) libre et open-source, et vous pouvez [trouver le code source ici sur notre dépôt](https://framagit.org/framasoft/peertube/mobile-application).

<div class="news-img-columns">
  <img src="/img/news/peertube-app/fr/Peertube-app-welcome.jpg" alt="image welcome page" />
  <img src="/img/news/peertube-app/fr/Peertube-app-video-player.jpg" alt="image player" />
</div>

#### La complexité du Fediverse simplifiée

Ce travail préparatoire nous a permis de réaliser qu’un client mobile était une **formidable opportunité de simplifier l’expérience PeerTube**. PeerTube n’est pas une plateforme vidéo : c’est un réseau de plateformes vidéo, chacune avec ses propres règles, moyens et objectifs, qui peuvent choisir de se fédérer avec d’autres (ou non).

Il est, de par sa conception, plus complexe qu’une plateforme centralisée. L’un des principaux commentaires que nous avons reçus de la part des passionnés de vidéo est le suivant

> Je ne sais pas où ouvrir un compte. Je ne sais pas où chercher et trouver des vidéos » (même si nous maintenons [SepiaSearch](https://sepiasearch.org/)).

![SepiaSearch homepage img](/img/news/peertube-app/fr/2024-12-sepia-search-screenshot-EN.jpg)

##### Compte local

Dans un client mobile, nous pouvons créer une sorte de compte local, directement sur votre appareil, afin que vous puissiez accéder à votre liste de visionnage, à vos listes de lecture, à vos favoris, etc. **Cela vous évite d’avoir à trouver une plateforme sur laquelle vous devez créer un compte** si vous voulez simplement profiter du contenu vidéo.

![image watch later](/img/news/peertube-app/fr/Peertube-app-watch-later.jpg)

##### Explorer les plateformes

Nous pouvons également inclure un moteur de recherche et une interface pour explorer la fédération des plateformes PeerTube et trouver des vidéos adaptées à vos centres d’intérêt. Tout le monde ne connaît pas l’existence de [SepiaSearch](https://sepiasearch.org/) (et d’autres moteurs de recherche fédérés) : **vous l’avez dès le départ, dans votre poche**.

![image explore](/img/news/peertube-app/fr/Peertube-app-explore.jpg)

##### Mise en évidence de la diversité des plateformes

Enfin, nous pouvons présenter le contenu d’une manière qui mette en évidence les plateformes et vous montrer où sont hébergées les vidéos/chaînes que vous regardez. La différenciation des plateformes est **un moyen pratique et visuel d’introduire le concept de fédération** auprès d’un public plus large.

![image explore platforms](/img/news/peertube-app/fr/Peertube-app-explore-2.jpg)

#### Designer pour sortir des dark patterns

Point humilité : une petite association française n’aura jamais la force de travail de Google ni l’argent d’Amazon (et vice versa). Mais **nous avons un avantage : nous ne sommes pas contraints par les règles du capitalisme de surveillance** et ses modèles de [captologie](https://fr.wikipedia.org/wiki/Captologie).

> Ni PeerTube ni l’application mobile n’ont intérêt à capter votre attention, à vous gaver de publicités et à vous soutirer des données comportementales et personnelles.

C’est ainsi que **nous libérons le design des conceptions toxiques tels que le « [doom scrolling](https://fr.wikipedia.org/wiki/Doomscrolling) » , la curation de flux, et les notifications omniprésentes**.

Cela peut sembler évident, mais il faut un réel effort pour concevoir une interface débarrassée de ce qui est malheureusement devenu la nouvelle norme. D’autant plus qu’il faut la rendre suffisamment familière pour qu’elle soit facile à utiliser.

![image see more](/img/news/peertube-app/fr/Peertube-app-voir-plus.jpg)

#### Une toute première version, limitée par les (play et i) stores

Nous savions à l’avance que **l’intégration dans le PlayStore de Google et l’AppStore d’Apple serait un défi**. Ils n’étaient manifestement pas prêts à héberger un client pour (non pas une plateforme mais) un réseau de plateformes autonomes de partage de vidéos, édité par une petite association française à but non lucratif, financée par son site web de dons indépendants.

Nous étions au courant des [problèmes rencontrés par Thorium](https://github.com/sschueller/peertube-android/issues/302) (un autre client mobile PeerTube). Nous avons reçu l’aide et les conseils de Gabe, qui développe [l’outil de streaming Owncast](https://owncast.online/) (que votre clavier repousse toujours les miettes et clique toujours en douceur), et qui a [rencontré de nombreux obstacles](https://laurenshof.online/owncast-and-the-app-store/)… Nous étions au courant de tout cela mais, oh mon Tux, quelle aventure.

Après avoir fait des pieds et des mains, nous y voilà, vous pouvez télécharger l’application mobile PeerTube ici :

<div style="display: flex; flex-wrap: wrap;  align-items: baseline; justify-content: center; gap: 1rem">
  <a href="https://f-droid.org/packages/org.framasoft.peertube/" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Get_it_on_F-Droid.png" alt="logo F-Droid"/></a>
  <a href="https://play.google.com/store/apps/details?id=org.framasoft.peertube" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Google-play111.png" alt="logo google store"/></a>
  <a href="https://apps.apple.com/fr/app/peertube/id6737834858" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/fr/Apple-store111.png" alt="logo App store" /></a>
</div>

<p class="text-center mt-3">
  <a target="_blank" rel="noopener noreferrer" href="https://asso.framasoft.org/dolo/h/peertube-apk-latest" class="jpt-secondary-button jpt-link-button ">Télécharger le fichier apk (Android/Expert⋅e)</a>
</p>

#### (dé)Limiter la fédération

Pour passer les processus de validation d’Apple (et, dans une moindre mesure, de Google), nous avons dû présenter l’application mobile avec une « liste autorisée » de plateformes PeerTube répondant à leurs normes.

Voici l’étant de ces limitations à l’heure actuelle :

* **L’AppStore d’Apple** : limité à une liste d’autorisation très stricte. A vrai dire, une semaine avant la sortie, nous ne sommes toujours pas sûrs d’être validés. Une fois que nous y serons parvenus, nous verrons comment élargir la liste et permettre aux utilisateurs d’ajouter les plateformes qu’ils souhaitent.
* **Google Play Store** : liste limitée, mais les utilisateurs peuvent déjà ajouter les plateformes qu’ils souhaitent. Nous prévoyons d’élargir la liste ensuite
* **F-Droid** et téléchargement direct de l’apk : toutes les plateformes PeerTube que nous avons indexées sur [SepiaSearch](https://sepiasearch.org/) sont disponibles. Si une instance n’est pas déclarée dans notre index ou est modérée, vous pouvez l’ajouter manuellement.

![image PeerTube plaforms img](/img/news/peertube-app/fr/Peertube-app-plaforms.jpg)

Nous n’insisterons jamais assez sur le fait que **leurs magasins ne sont pas prêts à accueillir des réseaux indépendants axés sur la solidarité**. Par exemple, un petit lien de donation « soutenez-nous » dans le pied de page de notre site web ou même sur l’une des plateformes autorisées a déclenché un « non » de la part d’Apple.

Et c’est cohérent : comme on l’a vu dans [leur combat avec Epic](https://en.wikipedia.org/wiki/Epic_Games_v._Apple) (propriétaire de Fortnite) Apple prend sa part dans chaque achat in-app. Ils ont un intérêt économique à garder vos dépenses enfermées dans leur écosystème. S’il vous plaît, s’il vous plaît : pensez à récupérer votre liberté ;).

![img déçu](/img/news/peertube-app/fr/expected_nothing_fr.jpg)

#### Bientôt, dans l’application PeerTube

Entrer dans les très petites cases d’Apple (et de Google) a demandé du temps et de l’énergie, plus que ce à quoi nous nous attendions. Nous avons décidé de publier une première version (incomplète) de l’application en décembre, et de l’améliorer progressivement.

Voici les **fonctionnalités que nous prévoyons de développer et de partager pour l’application PeerTube** :

* Bientôt (début 2025)
  * Finaliser et publier les rapports sur le design et la stratégie mobile
  * Publier la documentation
  * Lire une vidéo en arrière-plan
  * Se connecter à son compte, s’abonner, commenter des vidéos
  * Prochaine recommandation de vidéo
  * Améliorer la situation de la liste des plateformes limitées
* Ensuite (mi 2025 (si financé))
  * Adaptation aux tablettes
  * Adaptation aux téléviseurs (AndroidTV… AppleTV dépendra de leurs limitations)
  * Regarder hors ligne (pour les contenus téléchargeables)

Pour l’instant, nous attendons toujours le financement de ces fonctionnalités pour la mi-2024 (pour lesquelles nous avons demandé une subvention NLnet).

En fonction du succès et de l’utilisation de l’application, **nous aimerions ajouter le cas d’utilisation du créateur de contenu à l’application**. Mais ce n’est pas une mince affaire : télécharger et publier une vidéo, gérer son contenu, créer un livestream, etc. Nous nous demandons encore **où, quand et comment obtenir des fonds pour cette entreprise**.

![David Revoy Illustration](/img/news/peertube-app/fr/PeerTube-app-CC-BY-David-Revoy.jpeg)

#### Prendre soin, partager et contribuer !

**C’est ici que nous avons besoin de vous**.

Nous espérons que vous **apprécierez cette application, que vous la téléchargerez et l’utiliserez, et que vous la partagerez** avec vos amis. Il s’agit d’un nouveau moyen de promouvoir le contenu de PeerTube, d’attirer le public vers de fabuleux créateurs de contenu, de l’inciter à partager davantage et de relancer la boucle virale.

Cette application est également **un moyen de montrer comment les médias peuvent être présentés**, lorsqu’ils sont conçus avec soin pour votre agentivité et votre attention. Plus que jamais : **partager, c’est prendre soin**.

Vous pouvez également **contribuer en signalant des bugs** (dans l’application), en aidant au code ([voici le dépôt git](https://framagit.org/framasoft/peertube/mobile-application)), et en **traduisant l’interface**. Ce dernier point est important : pour l’instant, l’application n’est disponible qu’en anglais et en français. [**Vos contributions linguistiques sont les bienvenues** sur notre plateforme de traduction](https://weblate.framasoft.org/projects/peertube-app/peertube-app/).

Évidemment, nous prévoyons de maintenir l’application, d’ajouter des traductions, de corriger les bogues et d’effectuer des mises à jour de sécurité lorsque cela est nécessaire : mais cela a un coût. **Nous avons besoin de sécuriser le budget 2025 de Framasoft** pour pérenniser le poste de Wicklow dans notre équipe (ce qui est une priorité pour nous). **Notre campagne de dons est active en ce moment**, vous pouvez apporter votre soutien [ici](https://support.joinpeertube.org/) (et merci !).

![David Revoy Illustration 20 years](/img/news/peertube-app/fr/20-ans-CC-BY-David-Revoy.jpeg)

---


Vous pouvez nous aider à continuer à améliorer PeerTube en partageant cette information, en [proposant des idées d'améliorations](https://ideas.joinpeertube.org/) (en anglais) et, si vous pouvez vous le permettre, en [faisant un don à Framasoft](https://support.joinpeertube.org/), l'association qui développe PeerTube.

Merci d'avance de votre soutien !
Framasoft

<div style="display: flex; flex-wrap: wrap;  align-items: baseline; justify-content: center; gap: 1rem">
  <a href="https://f-droid.org/packages/org.framasoft.peertube/" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Get_it_on_F-Droid.png" alt="logo F-Droid"/></a>
  <a href="https://play.google.com/store/apps/details?id=org.framasoft.peertube" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/en/Google-play111.png" alt="logo google store"/></a>
  <a href="https://apps.apple.com/fr/app/peertube/id6737834858" style="max-width: 200px; height: auto;"><img src="/img/news/peertube-app/fr/Apple-store111.png" alt="logo App store" /></a>
</div>

<p class="text-center mt-3">
  <a target="_blank" rel="noopener noreferrer" href="https://asso.framasoft.org/dolo/h/peertube-apk-latest" class="jpt-secondary-button jpt-link-button ">Télécharger le fichier apk (Android/Expert⋅e)</a>
</p>
