---
id: isd-study
title: Déclaration concernant l'étude de l'ISD (Allemagne) sur PeerTube
date: December 21, 2022
---

Nous, Framasoft, développons le logiciel PeerTube depuis plus de 5 ans.

[Framasoft](https://framasoft.org) est une association française à but non lucratif de 38 membres (10 employés, 28 volontaires), PeerTube est l'un de nos 50 projets, et nous faisons tout ce travail avec un seul développeur (qui ne travaille pas à plein temps sur PeerTube). Veuillez noter que 2 ou 3 d'entre nous comprennent, plus ou moins couramment, l'allemand.

Le 19 décembre, un journaliste du [Tagesspiegel Background](https://background.tagesspiegel.de/) nous a informés qu'une étude réalisée par des chercheurs allemands et portant sur l'utilisation de PeerTube par l'extrême-droite était sur le point d'être publiée. Il nous a posé 3 questions sur l'ampleur du problème et ce qui pouvait être fait, et nous lui avons répondu le jour même.

Le 20 décembre, nous avons pu obtenir cette étude (elle est [disponible en ligne et en allemand ici](https://www.isdglobal.org/isd-publications/die-hydra-im-netz-herausforderung-der-extremistischen-nutzung-des-fediverse-am-beispiel-peertube/)) et la traduire. Voilà pourquoi il nous a fallu du temps pour écrire et publier une déclaration collective.

Tout d'abord, nous tenons à remercier les chercheuses et chercheurs de l'ISD pour leur travail. Plus nous connaîtrons la façon dont les manipulateurs conspirationniste et les fascistes utilisent PeerTube, plus les communautés PeerTube apprendront à se protéger efficacement de ces contenus.

Partager le savoir, c'est partager le pouvoir.


#### PeerTube dit non aux fascistes et aux manipulateurs de complots

Soyons clairs : les valeurs de Framasoft sont fondamentalement opposées au fascisme. Cela vaut également pour les manipulations conspirationnistes qui conduisent à blesser et à tuer des gens. ([voir notre manifeste, récemment publié](https://framasoft.org/fr/manifest/), qui énonce nos valeurs fondamentales).

Nous sommes d'accord avec les résultats de l'étude. D'après notre expérience, les fachos représentent une très, très petite part de la fédération PeerTube (appelée "vidiverse"), mais ils savent être des trolls très bruyants et énergivores.

Nous ne devons pas ignorer le fait que, en 2022, si une société produit des groupes fascistes et nazis, ce n'est pas à cause de la technologie, mais à cause d'un problème plus profond, qui est principalement lié à la complaisance de certains gouvernements avec les idées d'extrême droite. Néanmoins, il est également vrai que certains dispositifs techniques qui fonctionnent sur la base de la mesure d'audience et de la publicité sont les premiers à diffuser des idées extrémistes. Le Fediverse ne fonctionne pas ainsi, mais nous devons nous efforcer de le maintenir en bonne santé.

PeerTube est un logiciel libre, nous ne pouvons pas empêcher ces personnes de l'utiliser. L'Allemagne, la France et la plupart des démocraties modernes ont déjà introduit des lois qui peuvent amener la justice à condamner les administrateurs de PeerTube qui hébergent sciemment et volontairement des contenus haineux, dangereux et nazis.

Ce que nous pouvons faire (et [avons fait](https://joinpeertube.org/faq#does-peertube-offer-moderation-tools) et voulons continuer), c'est donner aux communautés PeerTube les outils pour modérer, se protéger et ostraciser les fascistes et les plateformes PeerTube dangereuses.

Nous "modérons" (c'est à dire : bannissons) les contenus fascistes de tous les outils que nous gérons. Ainsi, nous nettoyons régulièrement l'[index des instances](https://instances.joinpeertube.org/instances). Par exemple, nous avons retiré de cet index des instances problématiques, dont plusieurs sont allemandes. Cela n'empêche pas ces instances d'exister, mais au moins nous essayons de garder l'index sain.


#### Nous avons besoin que les communautés s'impliquent

L'étude le dit clairement : l'une des principales solutions à ce type de contenu repose sur la responsabilisation des communautés.

[PeerTube-isolation](https://peertube_isolation.frama.io/) est une liste de blocage communautaire qui est maintenue en totale indépendance de nous. [L'installation de leur plugin](https://framagit.org/framasoft/peertube/official-plugins/-/blob/master/peertube-plugin-auto-mute/README.md) sur votre plateforme PeerTube peut vous aider à vous assurer que vous ne vous fédérez pas avec des contenus dangereux.

Nous encourageons tous ceux qui veulent aider à contribuer au travail de la communauté PeerTube-Isolation. Nous retirons automatiquement les instances qu'ils et elles bloquent de l'index qui alimente [SepiaSearch](https://sepiasearch.org/), notre moteur de recherche PeerTube.

Notre objectif est de maintenir les contenus nuisibles et haineux isolés dans leur propre bulle de fédération. Isolés, ils peuvent faire autant de bruit qu'ils le souhaitent : ils ne contamineront pas les autres avec leurs merdes. Ils comprendront ainsi qu'ils ne sont pas les bienvenus dans les communautés de PeerTube, et n'auront aucun intérêt à essayer à s'y investir.

En outre, nous sommes toujours en train d'explorer de nouvelles façons d'aider les communautés et les administrateurs de PeerTube à modérer les discours de haine et les contenus dangereux. Si vous pensez à une fonctionnalité que nous pourrions ajouter à PeerTube pour aider à isoler de tels contenus, veuillez la partager sur [Let's Improve PeerTube](https://ideas.joinpeertube.org/).


#### Un appel aux administrateurs de plateformes PeerTube, et à les aider

Cela dit, un outil ne peut pas tout faire. 

Les administrateurs d'instance ont besoin d'aide, car ils ont plusieurs tâches à accomplir : l'administration du système (sauvegarde, mises à jour, etc.), la curation de contenu (est-ce que je veux héberger des vidéos ludo-éducatives ? des fictions originales ? des vidéos de créateurices Queer ?), les politiques de fédération (quelles plateformes dois-je suivre ? quels suivis dois-je accepter ?), les politiques de modération (et la modération peut se faire dans les choix de fédération, le contenu hébergé, les commentaires, etc.)

Il s'agit d'un travail très complexe, et il n'est pas recommandé d'être seul pour le mener à bien. C'est pourtant un travail crucial. Nous avons besoin que les administrateurs d'instances aient des règles concernant les plateformes avec lesquelles ils se fédèrent, afin qu'ils puissent appliquer leurs propres politiques et - espérons-le - arrêter la diffusion de contenus fascistes et dangereux. Mais c'est beaucoup de travail, et cela nécessite donc beaucoup d'aide.

Si vous voulez aider, essayez de rejoindre une équipe d'administration PeerTube. Proposez-leur votre aide. Signalez les contenus problématiques. Si les organisations publiques peuvent fournir une formation, une aide et des outils, cela peut toujours être utile ! Mais PeerTube est un [Commun](https://fr.wikipedia.org/wiki/Communs) : nous ne pouvons pas résoudre un tel problème si nous ne travaillons pas ensemble, en tant que Communauté (qui reste diverse et plurielle).


#### Partager le savoir, c'est partager le pouvoir

Les communautés PeerTube ont certainement besoin de plus de savoirs partagés sur les fascistes, leurs contenus, leurs méthodes, leurs arguments et sophismes, et comment s'en protéger.

Mais nous avons également besoin de plus de personnes qui aident et prennent les choses en charge, ensemble, en tant que communautés, car nous voulons que l'écosystème de PeerTube soit et reste sain.
