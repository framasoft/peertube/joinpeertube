import { createApp } from './main'

createApp(document.documentElement.lang)
  .then(({ app, router }) => {
    return router.isReady()
      .then(() => ({ app, router }))
  })
  .then(({ app }) => {
    console.log('Mounting client')

    app.mount('#app')
  })
