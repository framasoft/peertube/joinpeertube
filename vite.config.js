import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Markdown from 'vite-plugin-markdown'
import legacy from '@vitejs/plugin-legacy'
import { visualizer } from 'rollup-plugin-visualizer'
import { isoImport } from 'vite-plugin-iso-import'
import { markdownIt } from './src/news/news-markdown-it'
import svgLoader from 'vite-svg-loader'

export default defineConfig(({ ssrBuild }) => {
  let plugins = [
    vue(),
    svgLoader({
      // We already do it with appropriate options before
      svgo: false
    }),

    isoImport(),

    Markdown.plugin({
      mode: 'html',
      markdownIt
    })
  ]

  if (!ssrBuild) {
    plugins = plugins.concat([
      legacy({
        targets: [ 'defaults', 'last 4 iOS major versions' ]
      }),

      visualizer({ open: true })
    ])
  }

  return {
    base: process.env.BASE_URL,

    plugins
  }
})
